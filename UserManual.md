# VR2Normal User Manual

In this manual I will explain all the screens and options that the application has, it is not a step by step howto.

[How to re-encode virtual reality videos to normal using VR2Normal program](StepByStep.md)

[[_TOC_]]

## General Organization

The user interface of the application is divided into 2 large areas, each with its corresponding tabs. This is in order to be able to combine several of the tabs that need to be used together.

## Left Tabs

### Camera Editor

![Camera Editor](Screenshots/TabCameraEditor.jpg "Camera Editor")

This tab is the one that allows you to move the camera to add lines to the list of movements. It is intended to be used side by side with the [Movements](#movements) and [Input](#input) tabs. It is composed of 2 images and 2 control bars. The image at the top is a thumbnail of what the raw image we are adjusting looks like. The image below is the preview with all the options applied, it is a thumbnail of what you would see with the current camera options.

When a line is selected in the [Movements tab](#movements), changing the values updates the data for that line.

#### Time navigation bar

![Time Navigation Bar](Screenshots/TimeNavigationBar.png "Time navigation bar")

The buttons and editors bar just below the first image is in charge of moving the time of the video.

The editor in the middle shows which moment of the video we are watching in the preview images. We can edit it and go to any moment of the video. The buttons around it are shortcuts for easy movement.

- **-60s**: Set back 1 minute the time, default shortcut *Ctrl+Up*
- **-10s**: Set back 10 seconds the time, default shortcut *Up*
- **-1s**: Set back 1 second the time, default shortcut *Left*
- **-1f**: Set back the equivalent time of 1 frame, default shortcut *Ctrl+Left*
- **+1f**: Forward the time equivalent to 1 frame, default shortcut *Ctrl+Right*
- **+1s**: Forward 1 second the time, default shortcut *Right*
- **+10s**: Forward 10 seconds the time, default shortcut *Down*
- **+60s**: Forward 1 minute the time, default shortcut *Ctrl+Down*

For a better performance of the cache except for the frame by frame movement all the rest round to whole seconds and to values ending in 0. For example if we are at 00:01:05.500 the buttons would work as follows:

- ***-1s***: 00:01:05.000
- ***-10s***: 00:01:00.000
- ***-60s***: 00:01:00.000
- ***+1s***: 00:01:06.000
- ***+10s***: 00:01:10.000
- ***+60s***: 00:02:00.000

Frame by frame movement is not precise, it depends on the frames per second setting of the video.

#### Camera motion controls

![Camera Motion Controls](Screenshots/CameraMotionControls.png "Camera motion controls")

The controls at the bottom are all necessary to move the camera and add movements to the list.

- **Pitch**
    - Increasing its value moves the camera upward, default shortcut *W*
    - Reducing its value moves the camera downward, default shortcut *S*
- **Yaw**
    - Increasing its value moves the camera to the right, default shortcut *D*
    - Reducing its value moves the camera to the left, default shortcut *A*
- **Roll**
    - Increase its value rotates the camera counterclockwise, default shortcut *X*
    - Reducing Increase its value rotates the camera clockwise, default shortcut *C*
- **Fov**
    - Increasing its value reduces the camera zoom, default shortcut *Q*
    - Reducing its value increases the camera zoom, default shortcut *E*
- **Trans.**: Time in seconds of the duration of the transition from the previous camera position to this one. If 0, the camera changes instantly. It can be increased by pressing *R* and decreased by pressing *F*.
- **Text.**: Optional text to identify this camera angle. To start typing you can press *T* and then press *Enter* when finished.
- **V** and **H**: Vertical and horizontal displacement. It is applied to the resulting image, not really a camera movement. It is intended to center on the screen fixed elements, such as logos or texts.
- **Right Eye.**: If checked, the image corresponding to the right eye is used, otherwise the left eye is used. If the input video is 2d, this parameter is ignored. Can be checked and unchecked by pressing *Z*

### Sendcmd File

![Sendcmd File](Screenshots/TabSendcmdFile.jpg "Sendcmd file")

In this tab you configure where the file for sendcmd will be created. It is not necessary to use this tab, it is generated automatically when needed. It shows the file path at the top, the buttons to generate, save the file and the content.

### Clip Preview

![Clip Preview](Screenshots/TabClipPreview.jpg "Clip preview")

The usefulness of this tab is to configure and generate short previews of specific moments of the video, such as moments in which there is a transition to make sure it looks good without having to wait to process a much longer video. They can be of two types, as I mentioned before those of the transitions and specifying a time range.

This tab contains general formatting options, these options are explained in [Common format options](#common-format-options). Here I will only explain the specific options of this tab. In this tab are available formats that are only used in short videos, such as *gif* and *webp*.

- **Preview FPS**: Allows you to limit the frames per second of the preview, this speeds up the process. It is necessary to look for a suitable value depending on the video or codec used. For 60fps input video I recommend 30fps, and for gif format 12fps.
- **Transitions time added**: Time added before and after when generating a transition preview. It serves to give context to the transition.
- **Fragment Start**: Start time when a preview of a video fragment is generated.
- **Fragment End**: End time when a preview of a video fragment is generated.
- **Preview Dir**: Folder where the preview files will be saved, the concrete name will be generated automatically, including the name of the original file, the mode and the time of the original it represents.
- **Preview Transition**: When pressed, it generates the corresponding ffmpeg command to generate the preview of the selected transition in [Movements tab](#movements). If the *Run previews automatically* option is checked, the command is executed immediately.
- **Preview Fragment**: When pressed, it generates the ffmpeg command using the time range specified above. If the *Run previews automatically* option is checked, the command is executed immediately.
- **Command text**: Displays the last generated command for review or execute it independently of the program. For example, it can be used on a remote server.
- **Run**: When pressed it executes the last generated command.

### Comp. Preview

![Comp. Preview](Screenshots/TabCompletePreview.jpg "Complete preview")

This tab allows you to configure, generate and execute the necessary comma to generate a complete preview of the video. Contains general formatting options, these options are explained in [Common format options](#common-format-options). Here I will only explain the specific options of this tab.

- **Preview FPS**: Allows you to limit the frames per second of the preview, this speeds up the process. It is necessary to look for a suitable value depending on the video or codec used. For 60fps input video I recommend 30fps.
- **Preview File**: Here you can cover with the file name you want to generate. It is not necessary, a name is generated automatically, in the same folder as the input file.
- **New**: Allows you to browse the file system to select where and under what name should the file generated.
- **Generate ffmpeg command**: When pressed, it generates the ffmpeg command. If the *Run previews automatically* option is checked, the command is executed immediately.
- **Command text**: Displays the last generated command for review or execute it independently of the program. For example, it can be used on a remote server.
- **Run**: When pressed it executes the last generated command.

### Convert

![Convert](Screenshots/TabConvert.jpg "Convert")

This tab configures, generates and executes the command that performs the final conversion. Contains general formatting options, these options are explained in [Common format options](#common-format-options). Here I will only explain the specific options of this tab.

- **Output file**: Here you can cover with the file name you want to generate. It is not necessary, a name is generated automatically, in the same folder as the input file.
- **New**: Allows you to browse the file system to select where and under what name should the file generated by the conversion be created.
- **Generate ffmpeg command**: When pressed, it generates the ffmpeg command. If the *Run previews automatically* option is checked, the command is executed immediately.
- **Command text**: Displays the last generated command for review or execute it independently of the program. For example, it can be used on a remote server.
- **Run**: When pressed it executes the last generated command.

## Right Tabs

### Movements

![Movements](Screenshots/TabMovements.jpg "Movements")

This tab is the main part of the application together with the [Camera Editor tab](#camera-editor). In it we introduce the necessary camera movements needed to convert the video.

- **File**: This is the file that is currently being edited.
- **New**: Open the file browser to specify where to save the new movement file.
- **Open**: Open the file browser to select the motion file to be opened.
- **Add**: Add a new line, after the current one, to the motion list, with the current time data and camera settings in the [Camera Editor tab](#camera-editor).
- **Remove**: Deletes the selected line
- **Save**: Save the file

When a line is selected in the list, its data is automatically loaded into the [Camera Editor tab](#camera-editor), updating the preview as well.

- **Update Time**: When pressed it updates the time of the selected line with the one currently in the [Camera Editor tab](#camera-editor).
- **Sort**: When pressing sort the list using the time field, lines with time shorter than the previous line are not allowed. this allows to fix that problem.
- **Unselect**: Deselect the line. Allows you to use the [Camera Editor tab](#camera-editor) freely without it updating the list data.
- **Preview**: When pressed, it generates a preview of the selected line transition. Use the settings specified in the [Clip Preview tab](#Clip-Preview) tab.

### Screenshots

![Screenshots](Screenshots/TabScreenshots.jpg "Screenshots")

This tab allows you to take screenshots directly from the input file, to check the quality of the result and to adjust the resolution of the final process. It can also be used if simply want to get screenshots with the distortion corrected. It is designed to work together with the [Camera Editor tab](#camera-editor) because the captures are always taken with the current parameters.

- **Out. Dir**: Folder where the files of the screenshots are saved.
- **VR filter Resolution**: Resolution at which the distortion correction filter will render the image. This option is required.
- **Final Resolution**: Final resolution of the image, the output of the RV filter is rescaled to this resolution, if one of the parameters is 0 it is automatically calculated to respect the aspect ratio, if both are 0, this rescaling is disabled.
- **Format**: Format of the resulting screenshots, For quality comparison ***png*** or ***bmp*** is recommended.
- **Quality**: Image quality for lossy formats, each format has its own scale of values. For jpg values between 2 and 5 are recommended, with 2 being the highest quality. For webp acts as a percentage, 100 is the best quality, a value of 80 is recommended.
- **Take screenshot**: Pressing this button performs the screen capture using the parameters selected in the [Camera Editor tab](#camera-editor) and the options configured above.
- **Take screenshot at 720p**: Perform the capture the same as the previous button but using 1280x720 as the resolution of the VR filter.
- **Take screenshot at 1080p**: Perform the capture the same as the previous button but using 1920x1080 as the resolution of the VR filter.
- **Take screenshot at 720p rescaled to 1080p**: Perform the capture the same as the previous button but using 1280x720 as the resolution of the VR filter and 1920x1080 as the final resolution.

Screenshots take a short time to process, so a message will appear at the bottom indicating when they are being worked on and when they are finished. While they are in process the buttons are disabled.

### Input

![Input](Screenshots/TabInput.jpg "Input")

This tab contains all options related to the input video file. All these options are stored in the camera movement file. More information about some of these options can be found in the [ffmpeg filter manual](https://ffmpeg.org/ffmpeg-filters.html#v360).

- **Video File**: Location of the video file to be converted.
- **Format**: Geometric algorithm using the video to store a wide-angle image on a flat image.
    - *hequirect*: Half equirectangular projection. The most common for 180-degree videos.
    - *equirect*: Equirectangular projection. The most common for 360-degree videos.
    - *sg*: Stereographic format
    - *fisheye*: Fisheye projection
    - *equisolid*: Equisolid format
    - *pannini*: Pannini projection
- **Mode**: Input video stereo format.
    - **sbs**: Side by side. The two images in the video are placed side by side.
    - **tb**: Top bottom. The two images in the video are placed one on top of the other.
    - **2d**: 2D mono. The video is not 3D.
- **File FPS**: If the ffprobe option is configured, it is automatically read from the video file. It is only used for frame-by-frame movement through the video.
- **Diagonal FOV**: Diagonal field of view. This option has preference over the others, if it is different from 0 the others are not used, I recommend to leave it to 0 and use the vertical and horizontal.
- **Vertical FOV**: Vertical field of view. Normally has a value of 180.
- **Horizontal FOV**: Horizontal field of view. The most common values are 180, 200 and 360.
- **Video Length**: Video duration, it is used to not allow to exceed it in the previews, it can give ffmpeg errors if it is exceeded. It is read automatically if the ffprobe option is covered.
- **Force Mono Metadata**: Some input files may contain metadata that indicate how the players interpret them, this option is used to force the output to be mono. This option prevents copying the stereo option that some input files may contain.
- **Gen. Minutes Cache**: Generates the cache for all minutes, it is a background process, it does not interfere with normal operation, once generated it allows to move with the -60s and +60s buttons much faster. Generates one frame for every minute of video
- **Gen. 10 Seconds Cache**: Same as above but every 10s, so that moving with -10s and +10s is much faster, it generates 6 frames for every minute of video.

This tab is intended to be used in conjunction with the [Camera Editor tab](#camera-editor) to quickly identify the correct settings in the preview.

### Config

![Config](Screenshots/TabConfig.jpg "Config")

This tab contains the main configuration of the application, it is the first thing to cover when using it for the first time.

- **Ffmpeg Binary**: Location of the ffmpeg executable. This is the most important configuration option if it is not covered the program does not work.
- **Ffprobe Binary**: Location of the ffprobe executable. It is optional, but without it some parameters of the input video are not automatically covered, such as the total duration.
- **Video Player**: Location of the video player to play the generated previews. It is optional.
- **Prev. Interpol.**: Interpolation algorithm used by the Camera Editor tab to generate the preview image. I recommend *nearest* or *linear*, because in this case speed is more important than image quality.
- **Cache Format**: Format used to store images in RAM and caches.It works even with the caches disabled because at least one image must be stored in RAM.
    -*BMP*: Uncompressed format, it's faster, but the images require a lot of space. Only recommended with caches disabled. One 7K image occupies approximately 75MB.
    -*PNG*: Lossless compression, it is the intermediate option, it occupies quite a lot, but much less than BMP.
    -*JPG*: High compression, the slowest, but images take up only a little space. It is the default option, because although it may be slow it is safer for everyone.
- **RAM Cache**: Maximum size in megabytes of the frame cache in RAM. 0 To disable. Allows to return with much more speed to the frames that we have already seen before. When full, the oldest frames are automatically deleted.
    - *Clear*: Delete the contents of the cache.
    - *RAM Usage*: In MB, RAM memory currently in use by the cache.
- **Disk Cache Dir**: Folder where the disk cache files will be stored. This cache is persistent even if the program is closed and the folder is shared by all the files you are working on.
- **Preload Seconds**: Number of seconds for which to generate the cache automatically from the current time of the video. For example if it is at 5 and we are at minute 1, it will generate the cache for 1:01, 1:02, 1:03, 1:04 and 1:05. Set to 0 to deactivate.
- **Disk Cache**: Maximum size in megabytes of the frame cache in disk. 0 To disable. Allows to pregenerate the cache to move forward much faster. When it fills up, the oldest frames are erased
    - *Clear*: Delete the contents of the cache.
    - *RAM Usage*: In MB, Disk space currently in use by the cache.
- **Show framing lines**: Displays auxiliary lines to correctly frame the output image. They are displayed in the [Camera Editor tab](#camera-editor) in the preview image.

![Framing Lines](Screenshots/FramingLines.jpg "Framing lines")

- **Limit camera movement speed**: Limit camera movement using the keys to the maximum speed the camera can display.
- **Autosave movement file**: If checked, automatically saves the file with camera movements when closing the application.
- **Autosave configuration**: If checked, automatically saves the configuration when closing the application.
- **Save window settings in config**: If checked, saves the current state of the window in the configuration file, size, position, active tabs...
- **Run previews automatically**: If checked, the commands to generate previews are automatically executed. It will be explained in more detail where it is used.
- **Play previews automatically**: If checked, the previews are played automatically. You need the *Video Player* option to be set.
- **Save Config**: If pressed, saves the configuration now.

The Select buttons when pressed allow you to navigate through the file system to find the corresponding file.

### Shortcuts

![Shortcuts](Screenshots/TabShortcuts.jpg "Shortcuts")

In this tab you can configure all the keyboard shortcuts. To do it, click on the option and then press the desired key or key combinations and wait a couple of seconds.

## Common format options

![Format Options](Screenshots/FormatOptions.jpg "Format Options")

These options are the same in several tabs, so I am only going to explain them once.

- **Output video format**: There are 3 fields that define the video codec and the quality.
    - *Codec*
        - ***x265***: Newer and more advanced but slow codec.
        - ***x264***: The most widely used codec today.
        - ***Xvid***: Older codec, but very fast, the quality is not good, but it is adequate for previews.
        - ***NVENC H265***: H265 codec using NVidia graphics cards, to use it you need a card that supports this particular codec and a version of Ffmpeg compiled to support it. Faster than x265 but less efficient with file size, with the same quality it produces larger files, with the same file size the quality is lower, I only recommend it for previews.
        - ***NVENC H264***: The same as the previous option but for the H264 codec.
        - ***gif***: Only available for short videos.
        - ***webp***: Only available for short videos.
        - ***Custom***: When selected, use the *Custom format* field to customize the ffmpeg command.
    - *Preset* It is only used by the x265 and x264 codecs. It allows you to adjust the quality and compression speed, the slower the better the quality. *veryfast* is recommended for previews and *medium*, *slow* or *slower* for final re-encoding. NVENC doesn't supports all the options, only *fast*, *medium* and *slow*.
    - *Quality* This is a numerical value that represents the image quality we want to achieve. Each codec has its own scale and the values of one have nothing to do with the others. The smaller the number, the higher the quality we get.
        - For x265, x264 and NVENC, values between 18 and 24 are recommended.
        - For Xvid, values between 3 and 6 are recommended.
        - The gif format does not use this option
        - For webp, values between 50 and 100 are recommended.
- **Output audio format**
    - *Codec*
        - ***Copy***: Copies the input audio without modifying it, in this case the bitrate is ignored. Recommended
        - ***AAC***: Most used audio format today.
        - ***MP3***: Older format but very compatible, usually used with the *Xvid* video format.
        - ***Opus***:
        - ***Vorbis***:
        - ***None***: Generates an output file without audio.
    - *Bitrate* Number in kbps. It depends on the codec used, and the number of channels. Recommended value between 128 and 256.
- **Custom format**: This field allows you to customize the ffmpeg command. The following variables are available for composing the command. To use it you have to select Custom in *Output video format*.
    - ***%FFMPEG%***: Contains the value for *Ffmpeg Binary* in the [Config tab](#config).
    - ***%FFMPEG_GUI_OPTIONS%***: Parameters required to display the progress bar.
    - ***%START_TIME%***: When we do a preview this tells ffmpeg where to start. it is only necessary in previews, when not used its value is empty.
    - ***%INPUT%***: Input file parameter.
    - ***%TO_TIME%***: When we are in a short preview it tells ffmpeg when to end. When not in use its value is empty.
    - ***%SIMPLE_FILTER%***: Parameter with complete filter.
    - ***%RAW_FILTER%***: Raw value of the filter without ffmpeg parameter, can be used to add more filters before or after or use complex filters.
    - ***%FORMAT%***: Complete output format with video and audio options.
    - ***%VIDEO_FORMAT%***: Video only options.
    - ***%AUDIO_FORMAT%***: Audio only options.
    - ***%VIDEO_QUALITY%***: Value selected in the configuration form
    - ***%VIDEO_PRESET%***: Value selected in the configuration form
    - ***%VIDEO_CODEC%***: Value selected in the configuration form
    - ***%AUDIO_QUALITY%***: Value selected in the configuration form
    - ***%AUDIO_CODEC%***: Value selected in the configuration form
    - ***%OUTPUT%***: Output file configured in the form.
    - ***%OUT_FILE_NAME%***: Output file without the extension, it allows to add the extension we need.
    - **Example default value**: `%FFMPEG% %FFMPEG_GUI_OPTIONS% %START_TIME% %INPUT% %TO_TIME% %SIMPLE_FILTER% %FORMAT% %OUTPUT%`
    - **Example gif format value**: `%FFMPEG% %FFMPEG_GUI_OPTIONS% %START_TIME% %INPUT% %TO_TIME% -filter_complex "[0:v] %RAW_FILTER%,split [a][b]; [a] palettegen=stats_mode=single [p]; [b][p] paletteuse=new=1" %OUTPUT%`
    - **Example NVENC H265**: `%FFMPEG% %FFMPEG_GUI_OPTIONS% -hwaccel_output_format cuda %START_TIME% %INPUT% %TO_TIME% %SIMPLE_FILTER% -c:v hevc_nvenc -preset %VIDEO_PRESET% -cq %VIDEO_QUALITY% %AUDIO_FORMAT% %OUTPUT%`
    - **Example NVENC H264**: `%FFMPEG% %FFMPEG_GUI_OPTIONS% -hwaccel_output_format cuda %START_TIME% %INPUT% %TO_TIME% %SIMPLE_FILTER% -c:v h264_nvenc -preset %VIDEO_PRESET% -cq %VIDEO_QUALITY% -profile:v high %AUDIO_FORMAT% %OUTPUT%`
    - **Example linux optimus nvidia hardware encoding**: `/usr/bin/optirun %FFMPEG% %FFMPEG_GUI_OPTIONS% -hwaccel_output_format cuda %START_TIME% %INPUT% %TO_TIME% %SIMPLE_FILTER% -c:v h264_nvenc -preset %VIDEO_PRESET% -cq %VIDEO_QUALITY% -profile:v high %AUDIO_FORMAT% %OUTPUT%`
- **Video resolution**: Output image related options.
    - ***Width***: Number of pixels of width of the image we want to generate
    - ***Height***: Number of pixels of height of the image we want to generate
    - ***Interpolation***: Interpolation algorithm to be used to generate the image, for previews I recommend *nearest* or *linear*, for final re-encoding *lanzcos*.

## Processing

When a command is being processed, a bar appears at the bottom with the process data, a progress bar and a cancel button.

![Progress](Screenshots/Progress.jpg "Progress")

When the process is finished, the progress bar and the button disappear, displaying a message indicating whether the process was successful or not.

## Errors

### Processing

If there is an error, an additional tab *Error Log* may appear, with the command and the error it returned.

![Error Log](Screenshots/TabErrorLog.jpg "Error Log")

The **Clear Log** button clears the message and hides the tab.

### Incorrect Lines

When there are incorrect lines in the [Movements tab](#movements), they are marked in red as follows.

![Error Lines](Screenshots/ErrorLines.png "Error lines")

The number of the line and the cell with the incorrect value are red. These lines are treated as if they do not exist.

The most common errors in these lines are those related to time overlapping. For example in line 2 of the image you can see that the transition is too long and overlaps with the beginning of the next line. Line 5 is not sorted because the time is shorter than the previous line, it can be fixed by pressing the sort button, this does not mean that the line marked in red is the wrong one, it is only the first line in which the problem is detected.

When the preview and convert options are used while there are incorrect lines the following warning appears at the top of the application.

![Lines Warning](Screenshots/LinesWarning.png "Lines warning")

This warning will only disappear when the lines are fixed and when the sendcmd file is generated again.
