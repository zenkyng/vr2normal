# How to re-encode virtual reality videos to normal using VR2Normal program

I am not going to explain steps that I consider too basic, such as installing ffmpeg on the different platforms, each one has its own method.

[[_TOC_]]

## Download and install ffmpeg

You need at least the version 5, you can download it from the official ffmpeg page or from the package manager of your operating system. [Ffmpeg](https://ffmpeg.org/download.html)

## Download and install the application

On windows the application works as a portable program. You unzip the zip file and run it directly where it is.
On linux the .deb package is installed on the system, an entry should appear in the menu "VR2Normal".

## Configure the application

Click on the **Config** tab.

![Config](Screenshots/StepByStepConfig.jpg "Config")

The most important thing is to configure the path to ffmpeg, on linux if ffmpeg is installed on the system just put "ffpmeg" and it should work. On windows it is safest to specify the full path to "ffmpeg.exe". In the same way you have to configure ffprobe, it is optional, but highly recommended and as it comes with ffmpeg it is easy to do. The other options are not important. You can look in the [complete manual](UserManual.md#config) to learn more about these options.

## Create a new camera movement file

Go to the **Movements** tab and click on the **New"** button. Select the path and the name of the new file and accept.

![New](Screenshots/StepByStepNew.jpg "New")

## Configure an input video file

Now you have to configure the video file to be converted, to do this go to the **Input** tab and click on the **Select** button.

![Video File](Screenshots/StepByStepVideoFile.jpg "Video file")

To check that it works correctly we click on the buttons to move the time to reload the preview and we move to a time where the image is well appreciated to configure its parameters. If ffprobe is configured and working correctly the FPS and length options are covered automatically, but the rest we have to set them manually.

The example video is a 360 degree 3d video top bottom.

## Configure the sendcmd file

This step is normally not necessary, if everything goes well this file is automatically configured in the same path as the moves file adding **.cmd** as extension, but as it is very important I will explain how to configure it.

Now we select where to save the sendcmd file. Without this step the program is not able to generate previews or convert the video. This step may not be necessary if there is already a file covered, but if it is blank we have to click the **Save CMD file** button. It will open the file explorer in the same folder as the move file already with a covered name. We can simply accept. When it is working properly and there is data to export the tab will look like this.

![Sendcmd File](Screenshots/StepByStepSendcmdFile.jpg "Sendcmd File")

## Add the first line to the list of camera movements

Now we have to start working on telling the program how to position the camera at each moment of the video. Go to the **Movements** and **Camera Editor** tab. Click on the **Add** button.
As there is no line yet, 2 lines will be created automatically, the first one with time 00:00:00.000 and the last one with the total time of the input file. These lines are mandatory and removing them or modifying their times may cause the program to fail. All other lines are to be added in order of time between these 2 lines.

![New Line](Screenshots/StepByStepNewLine.jpg "New line")

## Gradually advance the video and add lines of movements

Now we have to advance the video from the beginning and move the camera and add lines until the video is finished.
To move the time and camera we can use the buttons on the screen or use the keyboard shortcuts, I recommend using the keys, it is faster. You can see the complete list of keys, and change them if you want, in the **Shortcuts** tab. To describe it simply, the camera is controlled with "wasd", the time with the arrow keys and adding a new line with "Insert".
I advance the video and when I see that what I want to see is out of shot, I go back to where it is still completely visible using the second by second movement, add a new line, advance a little and place the camera until it is as I want, and then I advance again faster and until the shot changes and repeat the process.

This is what a finished video would look like

![All Lines](Screenshots/StepByStepAllLines.jpg "All lines")

## Previews

As the on-screen preview cannot show the transitions, to see if they look good, we have the **Clip Preview** tab. From it we can create short videos of a few seconds where we can see the camera transitions of the selected line in the **Movements** tab.
To use it we have to select a line and then click on the **Preview Transition** button and then **Run** or execute by ourselves the command it generates.

![Preview](Screenshots/StepByStepPreview.jpg "Preview")

You can also create longer previews by specifying the time range or complete in the **Comp. Preview** tab.

## Execute the conversion process

Once we are satisfied with our work moving the camera we have to go to the **Convert** tab, configure the output video options, generate the command and run it on our own or press the **Run** button.
When a process is executed with the Run button, a bar appears at the bottom with the process data, a progress bar and a cancel button.

![Processing](Screenshots/StepByStepProcessing.jpg "Processing")

When finished you can check the output file and see if everything is correct
