# VR2Normal

This program allows you to convert a 3D VR video into a normal video, using a virtual camera to select the most relevant parts of the video at different times. It only needs ffmpeg to work.

It allows you to see all the ffmpeg commands it generates so you can adapt them to your needs.

## Requirements

This program needs at least version 5 of ffmpeg to work. For both linux and windows, it is not necessary to install ffmpeg on the system. If your linux distribution does not have version 5 available, you can download it from the following pages, uncompress it and then configure it in the program.

- [For linux https://johnvansickle.com/ffmpeg/](https://johnvansickle.com/ffmpeg/)
- [For windows https://www.gyan.dev/ffmpeg/builds/](https://www.gyan.dev/ffmpeg/builds/)
- [For windows and linux https://github.com/BtbN/FFmpeg-Builds/releases](https://github.com/BtbN/FFmpeg-Builds/releases)

### Windows binaries

Only 64bit version is available. This is because I could not find 32bit ffmpeg binaries. All other libraries are included in the zip file.

### Debian/Ubuntu deb package

The deb package has the following dependencies ffmpeg >= 5.0.0, libqt5core5a >= 5.15.0, libqt5gui5 >= 5.15.0

### Compiling

The compilation process is the same as this other program I made. Obviously changing the repositories and names.

[Compilation process on linux](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt/-/blob/main/compile_linux.md)

## User Manual

[All program options are explained in the user manual](UserManual.md)

## Step by step howto

[How to re-encode virtual reality videos to normal using VR2Normal program](StepByStep.md)

## Screenshot

![Cover](Screenshots/Cover.png "Cover screenshot")
