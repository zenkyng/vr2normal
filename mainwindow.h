#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QShortcut>
#include <QSettings>
#include <QJsonDocument>
#include <QTableWidgetItem>
#include <QUndoStack>
#include <QCache>
#include <QMap>
#include <QQueue>
#include <QTimer>
#include <QElapsedTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

struct tableline_t {
	int Time;
	double Transition;
	double Pitch;
	double Yaw;
	double Roll;
	double FOV;
	bool RightEye;
	double V;
	double H;
	QString Text;
	bool Valid;
};

class MainWindow : public QMainWindow {
		Q_OBJECT

	public:
		MainWindow(QWidget *parent = nullptr);
		~MainWindow();

	private:
		Ui::MainWindow *ui;
		QPixmap RawPixmap;
		QPixmap ScaledRawPixmap;
		QPixmap FilterPixmap;
		QPixmap ScaledFilterPixmap;
		QProcess FfmpegRaw;
		QProcess FfmpegFilter;
		QProcess Ffprobe;
		QByteArray RawData;
		QByteArray FilterResultData;
		QByteArray PreviewErrorData;
		QString ProcessErrorString;
		QCache<QTime, QByteArray> RawCache;
		QTime CurrentRawTime;
		bool FileOk;
		QString CurrentFilename;
		QString CurrentFilenameHash;
		QString CurrentFileDir;
		bool NeedFilterUpdate;
		bool ForceDetectVideo;

		QJsonDocument VideoInfo;

		QUndoStack *TableUndo;

		QShortcut *PitchUP;
		QShortcut *PitchDown;
		QShortcut *YawUP;
		QShortcut *YawDown;
		QShortcut *RollUP;
		QShortcut *RollDown;
		QShortcut *FovUP;
		QShortcut *FovDown;
		QShortcut *FocusText;
		QShortcut *TranUP;
		QShortcut *TranDown;
		QShortcut *VUP;
		QShortcut *VDown;
		QShortcut *HUP;
		QShortcut *HDown;

		QShortcut *RowUp;
		QShortcut *RowDown;

		QShortcut *Undo;
		QShortcut *Redo;

		QShortcut *Unselect;

		QProcess Execution;

		bool FfmpegRawCanceled;
		bool FfmpegFilterCanceled;
		bool ExecutionCanceled;

		QSettings *Settings;

		QProcess DiskCacheProcess;
		QMap<QTime, int> OndemandDiskCacheQueue;
		QQueue<QTime> DiskCacheQueue;

		QTimer OndemandCacheTimer;
		QTimer RemoveCacheTimer;

		QProcess ScreenshotProcess;
		QString PreviewFile;
		//		QElapsedTimer TimeCounter;

		bool Debug;
		QElapsedTimer RawElapsedTimer;
		QElapsedTimer FilterElapsedTimer;
		qint64 RawStartTime;
		qint64 RawReadTime;
		qint64 RawPrevirewTime;
		qint64 RawTotalTime;
		qint64 FilterStartTime;
		qint64 FilterSendTime;
		qint64 FilterReadTime;
		qint64 FilterPrevirewTime;
		qint64 FilterTotalTime;

	protected:
		void closeEvent(QCloseEvent *event) override;
		void resizeEvent(QResizeEvent *event) override;
		void UpdateTableTime();
		void UpdateItem(int column, QString value);
		int GetSelectedRow();
		QString Row2TSV(int row);
		QStringList Table2TSV();
		void LoadTable();
		bool BlockRefresh;
		QString ReadItemValue(int Row, int Column, const QString DefaultValue = "");
		QTableWidgetItem *ReadItem(int Row, int Column);
		tableline_t ReadRow(int Row);
		tableline_t LocateRow(const QTime &Time);
		QString ProcessProgram;
		QStringList ProcessParams;
		QTime ProcessTotalTime;
		bool ProcessIsPreview;
		void TableUpSelect();
		void TableDownSelect();
		void ShowProcessError(QProcess *process, QString CustomErrorLog = "");
		void CleanTable();

		bool CheckValidTableTime(int Row, QTime &Time);

		void LoadShortcuts();
		void SaveShortcuts();
		void ApplyShortcuts();

		QString ComposeFfmpegCommand(QString Output, QString Template, QString VideoCodec, int VideoQuality,
									 QString VideoPreset, QString AudioCodec, int AudioQuality, int StartMS, int EndMS, double FPS, QString V360Format,
									 QString V360Mode, double V360IDFOV, double V360IVFOV, double V360IHFOV, double V360DFOV, double V360Pitch,
									 double V360Yaw, double V360Roll, double V360VOffset, double V360HOffset, bool V360RightEye, int V360W, int V360H,
									 QString V360Interp, bool V360ResetRot, bool ForceMomoMetadata);

		QString GetTemplate(QString Format);
		QString GetExtension(QString Format);

		void DisableRunButtons();

		void GenerateDiskCache(int seconds);
		void GenerateDiskCache(int seconds, QTime start, QTime end, bool ondemand);
		qint64 DiskCacheUsage();

		void TakeScreenshot(QTime Time, int V360W, int V360H, int FinalW, int FinalH);
		void EnableScreenshotsButtons(bool Enable);
		void PrintFramingLines();

	public slots:
		void ReloadRawImage(const QTime &time);
		void ReloadFilterImage();
		void ReadRawData();
		void ReadFilterData();
		void StartFilterProcess();
		void GenerateRawImageTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void GenerateFilterImageTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void FfprobeTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void ReadProcessData();
		void ProcessTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void UndoSlot();
		void RedoSlot();
		void UnselectSlot();
		void EditTextFocus();
		void ExecuteProcess();
		void DiskCacheProcessTerminate (int exitCode, QProcess::ExitStatus exitStatus);
		void CleanDiskCache(qint64 size);
		void DiskCacheStart();
		void RemoveCacheTimeout();
		void ScreenshotProcessTerminate (int exitCode, QProcess::ExitStatus exitStatus);

		void PitchUPEvent();
		void PitchDownEvent();
		void YawUPEvent();
		void YawDownEvent();
		void RollUPEvent();
		void RollDownEvent();
		void FovUPEvent();
		void FovDownEvent();
		void VUPEvent();
		void VDownEvent();
		void HUPEvent();
		void HDownEvent();

	private slots:
		void on_NextSecond_clicked();
		void on_PrevSec_clicked();
		void on_Next10Sec_clicked();
		void on_Prev10Sec_clicked();
		void on_NextMinute_clicked();
		void on_PrevMinute_clicked();
		void on_SelectFile_clicked();
		void on_fileLineEdit_textChanged(const QString &arg1);
		void on_Pitch_valueChanged(double arg1);
		void on_Yaw_valueChanged(double arg1);
		void on_Roll_valueChanged(double arg1);
		void on_Fov_valueChanged(double arg1);
		void on_RightEye_stateChanged(int arg1);
		void on_AddLine_clicked();
		void on_Unselect_clicked();
		void on_RemoveLine_clicked();
		void on_UpdateTime_clicked();
		void on_Save_clicked();
		void on_Table_itemSelectionChanged();
		void on_NextFrame_clicked();
		void on_PrevFrame_clicked();
		void on_OpenMovementFile_clicked();
		void on_SaveCMD_clicked();
		void on_GenerateCMD_clicked();
		void on_PreviewTransition_clicked();
		void on_PreviewFragment_clicked();
		void on_NewMovement_clicked();
		void on_SelectOutFile_clicked();
		void on_OutGenerateCommand_clicked();
		void on_formatComboBox_currentTextChanged(const QString &arg1);
		void on_modeComboBox_currentTextChanged(const QString &arg1);
		void on_inputFOVSpinBox_valueChanged(int arg1);
		void on_VFovSpinBox_valueChanged(int arg1);
		void on_HFovSpinBox_valueChanged(int arg1);
		void on_GUIPreviewInterpolation_currentTextChanged(const QString &arg1);
		void on_FfmpegBinSelect_clicked();
		void on_ClearErrorLog_clicked();
		void on_VideoLength_userTimeChanged(const QTime &time);
		void on_SelectVideoPlayer_clicked();
		void on_SelectFfprobe_clicked();
		void on_Sort_clicked();
		void on_actionSave_screen_triggered();
		void on_SaveConfig_clicked();
		void on_Trans_valueChanged(double arg1);
		void on_Text_editingFinished();
		void on_Cancel_clicked();
		void on_CPreviewGenerateFfmpeg_clicked();
		void on_PreviewCurrentLine_clicked();
		void on_SelectPreviewFile_clicked();
		void on_RAMCache_valueChanged(int arg1);
		void on_VOffset_valueChanged(double arg1);
		void on_HOffset_valueChanged(double arg1);
		void on_GenerateDiskCache_clicked();
		void on_Gen10SCache_clicked();
		void on_ClearDiskCache_clicked();
		void on_ClearRAMCache_clicked();
		void on_DiskCacheDirSelect_clicked();
		void on_TakeScreenshot1920_clicked();
		void on_TakeScreenshot1280_clicked();
		void on_TakeScreenshot_clicked();
		void on_TakeScreenshotRescaled_clicked();
		void on_ShowFramingLines_stateChanged(int arg1);
		void on_PreviewDirSelect_clicked();
		void on_pushButton_clicked();
		void on_EnableDebug_stateChanged(int arg1);
};
#endif // MAINWINDOW_H
