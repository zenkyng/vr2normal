﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QStringList>
#include <QPixmap>
#include <QFileDialog>
#include <QTextStream>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonArray>
#include <QMenu>
#include <QBrush>
#include <QPushButton>
#include <QUndoCommand>
#include <QUndoGroup>
#include <QByteArray>
#include "qtablewidgetundo.h"
#include <QCryptographicHash>
#include <QDirIterator>
#include <QPainter>
#include <QTimer>

#define LEFT_TAB_CAMERA 0
#define LEFT_TAB_SENDCMD 1
#define LEFT_TAB_CLIP_PREVIEW 2
#define LEFT_TAB_COMPLETE_PREVIEW 3
#define LEFT_TAB_CONVERT 4
#define LEFT_TAB_ERROR_LOG 5

#define RIGHT_TAB_MOVEMENTS 0
#define RIGHT_TAB_SCREENSHOTS 1
#define RIGHT_TAB_INPUT 2
#define RIGHT_TAB_CONFIG 3
#define RIGHT_TAB_SHORTCUTS 4
#define RIGHT_TAB_DEBUG 5

//TODO: Remove metadata from source video "-metadata:s:v stereo_mode=mono"

tableline_t DefaultTableLine = {0, 0, 0, 0, 0, 0, false, 0, 0, "", false};

void WriteFile(QString Filename, QString Content) {
	QFile file(Filename);
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&file);
	out << Content;
	file.close();
}

QString ReadFile(QString Filename) {
	QFile file(Filename);
	if (!file.open(QFile::ReadOnly | QFile::Text)) return "";
	QTextStream in(&file);
	return in.readAll();
}

QString ScapeFFFilters(QString in) {
	return QDir::toNativeSeparators(in).replace("\\", "\\\\").replace(":", "\\:").replace("%", "\\%").replace(",", "\\,");
}

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow) {
	ui->setupUi(this);

	ui->ErrorLines->setVisible(false);
	ui->RunState->setVisible(false);
	ui->ProgressArea->setVisible(false);

	DisableRunButtons();

	TableUndo = new QUndoStack(this);

	#ifdef Q_OS_WIN
	Settings = new QSettings(QCoreApplication::applicationDirPath() + QDir::separator() + "VR2Normal.ini",
							 QSettings::IniFormat);
	#else
	Settings = new QSettings(QCoreApplication::applicationName(), "config");
	#endif

	ui->FfmpegBin->setText(Settings->value("ffmpegbin", "ffmpeg").toString());
	ui->FfprobeBin->setText(Settings->value("ffprobebin", "ffprobe").toString());
	ui->VideoPlayer->setText(Settings->value("VideoPlayer", "").toString());
	ui->AutoRun->setChecked(Settings->value("AutoRun", "0").toBool());
	ui->AutoPlay->setChecked(Settings->value("AutoPlay", "0").toBool());
	ui->AutoSaveConfig->setChecked(Settings->value("AutoSaveConfig", "0").toBool());
	ui->AutoSaveMovement->setChecked(Settings->value("AutoSaveMovement", "0").toBool());
	ui->SaveWindowSettings->setChecked(Settings->value("SaveWindowSettings", "0").toBool());
	ui->GUIPreviewInterpolation->setCurrentText(Settings->value("GUIPreviewInterpolation", "nearest").toString());
	ui->RAMCache->setValue(Settings->value("RAMCache", "0").toInt());
	ui->DiskCache->setValue(Settings->value("DiskCache", "0").toInt());
	ui->DiskCacheDir->setText(Settings->value("DiskCacheDir", QDir::tempPath() + "/VR2Normal").toString());
	ui->PreloadCacheSeoconds->setValue(Settings->value("PreloadCacheSeoconds", "0").toInt());
	ui->CacheFormat->setCurrentText(Settings->value("CacheFormat", "JPG").toString());
	ui->ShowFramingLines->setChecked(Settings->value("ShowFramingLines", "0").toBool());
	ui->LimitMovement->setChecked(Settings->value("LimitMovement", "0").toBool());

	ui->PreviewVideoFormat->setCurrentText(Settings->value("PreviewVideoFormat", "Xvid").toString());
	ui->PreviewPreset->setCurrentText(Settings->value("PreviewPreset", "veryfast").toString());
	ui->PreviewCRF->setValue(Settings->value("PreviewCRF", "28").toInt());
	ui->PreviewAudioFormat->setCurrentText(Settings->value("PreviewAudioFormat", "None").toString());
	ui->PreviewAudioBitrate->setValue(Settings->value("PreviewAudioBitrate", "128").toInt());
	ui->PreviewCustomFormat->setText(Settings->value("PreviewCustomFormat", "").toString());
	ui->PreviewW->setValue(Settings->value("PreviewW", "852").toInt());
	ui->PreviewH->setValue(Settings->value("PreviewH", "480").toInt());
	ui->PreviewFPS->setValue(Settings->value("PreviewFPS", "30").toDouble());
	ui->PreviewInterpolation->setCurrentText(Settings->value("PreviewInterpolation", "linear").toString());
	ui->PreviewDir->setText(Settings->value("PreviewDir", "").toString());
	ui->PreviewTimeBefore->setValue(Settings->value("PreviewTimeBefore", "2000").toInt());
	ui->PreviewTimeAfter->setValue(Settings->value("PreviewTimeAfter", "2000").toInt());

	ui->CPreviewVideoFormat->setCurrentText(Settings->value("CPreviewVideoFormat", "Xvid").toString());
	ui->CPreviewPreset->setCurrentText(Settings->value("CPreviewPreset", "veryfast").toString());
	ui->CPreviewCRF->setValue(Settings->value("CPreviewCRF", "28").toInt());
	ui->CPreviewAudioFormat->setCurrentText(Settings->value("CPreviewAudioFormat", "None").toString());
	ui->CPreviewAudioBitrate->setValue(Settings->value("CPreviewAudioBitrate", "128").toInt());
	ui->CPreviewCustomFormat->setText(Settings->value("CPreviewCustomFormat", "").toString());
	ui->CPreviewW->setValue(Settings->value("CPreviewW", "852").toInt());
	ui->CPreviewH->setValue(Settings->value("CPreviewH", "480").toInt());
	ui->CPreviewFPS->setValue(Settings->value("CPreviewFPS", "30").toDouble());
	ui->CPreviewInterpolation->setCurrentText(Settings->value("CPreviewInterpolation", "linear").toString());

	ui->OutVideoFormat->setCurrentText(Settings->value("OutVideoFormat", "x265").toString());
	ui->OutPreset->setCurrentText(Settings->value("OutPreset", "medium").toString());
	ui->OutCRF->setValue(Settings->value("OutCRF", "24").toInt());
	ui->OutAudioFormat->setCurrentText(Settings->value("OutAudioFormat", "Copy").toString());
	ui->OutAudioBitrate->setValue(Settings->value("OutAudioBitrate", "128").toInt());
	ui->OutW->setValue(Settings->value("OutW", "1920").toInt());
	ui->OutH->setValue(Settings->value("OutH", "1080").toInt());
	ui->OutCustomFormat->setText(Settings->value("OutCustomFormat", "").toString());
	ui->OutInterpolation->setCurrentText(Settings->value("OutInterpolation", "lanczos").toString());

	ui->ScreenshotsDir->setText(Settings->value("ScreenshotsDir", "").toString());

	if (Settings->value("MovementFile", "").toString() != "") {
		if (QFile::exists(Settings->value("MovementFile", "").toString())) {
			ui->MovementFile->setText(Settings->value("MovementFile", "").toString());
			LoadTable();
		}
	}

	if (ui->DiskCacheDir->text() == "") { ui->DiskCache->setValue(0); }

	LoadShortcuts();

	Settings->beginGroup("MainWindow");
	if (Settings->contains("size"))
		resize(Settings->value("size", QSize(400, 400)).toSize());
	if (Settings->contains("pos"))
		move(Settings->value("pos", QPoint(200, 200)).toPoint());
	if (Settings->value("fullscreen", "0").toBool())
		showFullScreen();
	ui->LeftTabs->setCurrentIndex(Settings->value("LeftTabs", "0").toInt());
	ui->RightTabs->setCurrentIndex(Settings->value("RightTabs", "0").toInt());
	ui->Table->setColumnWidth(TIME_COLUMN, Settings->value("TimeRowSize", "100").toInt());
	ui->Table->setColumnWidth(TRANSITION_COLUMN, Settings->value("TransitionRowSize", "60").toInt());
	ui->Table->setColumnWidth(PITCH_COLUMN, Settings->value("PitchRowSize", "60").toInt());
	ui->Table->setColumnWidth(YAW_COLUMN, Settings->value("YawRowSize", "60").toInt());
	ui->Table->setColumnWidth(ROLL_COLUMN, Settings->value("RollRowSize", "60").toInt());
	ui->Table->setColumnWidth(FOV_COLUMN, Settings->value("FovRowSize", "60").toInt());
	ui->Table->setColumnWidth(EYE_COLUMN, Settings->value("EyeRowSize", "60").toInt());
	ui->Table->setColumnWidth(V_OFFSET_COLUMN, Settings->value("VOffsetSize", "60").toInt());
	ui->Table->setColumnWidth(H_OFFSET_COLUMN, Settings->value("HOffsetSize", "60").toInt());
	ui->Table->setColumnWidth(TEXT_COLUMN, Settings->value("TextRowSize", "200").toInt());
	Settings->endGroup();

	QObject::connect(ui->RawTime, &QTimeEdit::userTimeChanged, this, &MainWindow::ReloadRawImage);

	QObject::connect(&FfmpegRaw, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::GenerateRawImageTerminate);
	QObject::connect(&FfmpegRaw, &QProcess::readyReadStandardOutput, this, &MainWindow::ReadRawData);

	QObject::connect(&FfmpegFilter,  QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::GenerateFilterImageTerminate);
	QObject::connect(&FfmpegFilter, &QProcess::started, this, &MainWindow::StartFilterProcess);
	QObject::connect(&FfmpegFilter, &QProcess::readyReadStandardOutput, this, &MainWindow::ReadFilterData);

	QObject::connect(&Ffprobe, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::FfprobeTerminate);

	QObject::connect(&DiskCacheProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::DiskCacheProcessTerminate);

	QObject::connect(&ScreenshotProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::ScreenshotProcessTerminate);

	QObject::connect(ui->Pitch, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImage);
	QObject::connect(ui->Yaw, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImage);
	QObject::connect(ui->Roll, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImage);
	QObject::connect(ui->Fov, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImage);
	QObject::connect(ui->VOffset, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImage);
	QObject::connect(ui->HOffset, &QDoubleSpinBox::textChanged, this, &MainWindow::ReloadFilterImage);
	QObject::connect(ui->RightEye, &QCheckBox::stateChanged, this, &MainWindow::ReloadFilterImage);

	QObject::connect(PitchUP, &QShortcut::activated, this, &MainWindow::PitchUPEvent);
	QObject::connect(PitchDown, &QShortcut::activated, this, &MainWindow::PitchDownEvent);
	QObject::connect(YawUP, &QShortcut::activated, this, &MainWindow::YawUPEvent);
	QObject::connect(YawDown, &QShortcut::activated, this, &MainWindow::YawDownEvent);
	QObject::connect(RollUP, &QShortcut::activated, this, &MainWindow::RollUPEvent);
	QObject::connect(RollDown, &QShortcut::activated, this, &MainWindow::RollDownEvent);
	QObject::connect(FovUP, &QShortcut::activated, this, &MainWindow::FovUPEvent);
	QObject::connect(FovDown, &QShortcut::activated, this, &MainWindow::FovDownEvent);
	QObject::connect(FocusText, &QShortcut::activated, this, &MainWindow::EditTextFocus);
	QObject::connect(TranUP, &QShortcut::activated, ui->Trans, &QDoubleSpinBox::stepUp);
	QObject::connect(TranDown, &QShortcut::activated, ui->Trans, &QDoubleSpinBox::stepDown);
	QObject::connect(VUP, &QShortcut::activated, this, &MainWindow::VUPEvent);
	QObject::connect(VDown, &QShortcut::activated, this, &MainWindow::VDownEvent);
	QObject::connect(HUP, &QShortcut::activated, this, &MainWindow::HUPEvent);
	QObject::connect(HDown, &QShortcut::activated, this, &MainWindow::HDownEvent);

	QObject::connect(RowUp, &QShortcut::activated, this, &MainWindow::TableUpSelect);
	QObject::connect(RowDown, &QShortcut::activated, this, &MainWindow::TableDownSelect);

	QObject::connect(Undo, &QShortcut::activated, this, &MainWindow::UndoSlot);
	QObject::connect(Redo, &QShortcut::activated, this, &MainWindow::RedoSlot);

	QObject::connect(Unselect, &QShortcut::activated, this, &MainWindow::UnselectSlot);

	QObject::connect(ui->SaveConfigKS, &QPushButton::clicked, this, &MainWindow::SaveShortcuts);
	QObject::connect(ui->ApplyKS, &QPushButton::clicked, this, &MainWindow::ApplyShortcuts);

	QObject::connect(&Execution, &QProcess::readyReadStandardError, this, &MainWindow::ReadProcessData);
	QObject::connect(&Execution,  QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
					 this, &MainWindow::ProcessTerminate);

	QObject::connect(ui->CPreviewRun, &QPushButton::clicked, this, &MainWindow::ExecuteProcess);
	QObject::connect(ui->ExecutePreviewCommand, &QPushButton::clicked, this, &MainWindow::ExecuteProcess);
	QObject::connect(ui->ExecuteOutCommand, &QPushButton::clicked, this, &MainWindow::ExecuteProcess);

	QObject::connect(&OndemandCacheTimer, &QTimer::timeout, this, &MainWindow::DiskCacheStart);
	QObject::connect(&RemoveCacheTimer, &QTimer::timeout, this, &MainWindow::RemoveCacheTimeout);

	ui->LeftTabs->setTabVisible(LEFT_TAB_ERROR_LOG, false);
	ui->RightTabs->setTabVisible(RIGHT_TAB_DEBUG, false);

	ui->CurrentDiskUsage->setText(QString::number((double)DiskCacheUsage() / 1024 / 1024, 'f', 2));

	BlockRefresh = false;
}

MainWindow::~MainWindow() {
	if (ui->AutoSaveMovement->isChecked()) on_Save_clicked();
	if (ui->AutoSaveConfig->isChecked()) on_SaveConfig_clicked();
	delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event) {
	if (Execution.state() != QProcess::NotRunning) {
		QMessageBox::StandardButton Result = QMessageBox::question(this, "Close Confirmation",
											 "There are processes in progress, if you close them they will be cancelled.\nDo you want to close the application anyway?",
											 QMessageBox::Yes | QMessageBox::No);

		if (Result == QMessageBox::Yes) {
			event->accept();
		} else {
			event->ignore();
		}
	}
}

void MainWindow::LoadTable() {

	TableUndo->clear();

	CleanTable();

	if (!QFile::exists(ui->MovementFile->text())) return;
	QString Data = ReadFile(ui->MovementFile->text());
	QStringList DataLines = Data.split("\n");

	QRegularExpression
	TableLine("^([0-9.:]+)[[:space:]]+([0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([0-9.]+)[[:space:]]+([01]+)[[:space:]]*([^\\t]*)$");
	QRegularExpression
	OffsetLine("^([0-9.:]+)[[:space:]]+([0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([0-9.]+)[[:space:]]+([01]+)[[:space:]]+([\\-0-9.]+)[[:space:]]+([\\-0-9.]+)[[:space:]]*([^\\t]*)$");

	QRegularExpression File("^#File: (.*)$");
	QRegularExpression Format("^#Format: (.*)$");
	QRegularExpression Mode("^#Mode: (.*)$");
	QRegularExpression FOV("^#FOV: (.*)$");
	QRegularExpression VFOV("^#VFOV: (.*)$");
	QRegularExpression HFOV("^#HFOV: (.*)$");
	QRegularExpression FPS("^#FPS: (.*)$");
	QRegularExpression CMDFile("^#CMDFile: (.*)$");
	QRegularExpression VideoLength("^#VideoLength: (.*)$");
	QRegularExpression ForceMonoMetadata("^#ForceMonoMetadata: (.*)$");

	QRegularExpression CurrentTime("^#CurrentTime: (.*)$");
	QRegularExpression CurrentPitch("^#CurrentPitch: (.*)$");
	QRegularExpression CurrentYaw("^#CurrentYaw: (.*)$");
	QRegularExpression CurrentRoll("^#CurrentRoll: (.*)$");
	QRegularExpression CurrentFOV("^#CurrentFOV: (.*)$");
	QRegularExpression CurrentEye("^#CurrentEye: (.*)$");
	QRegularExpression CurrentVOffset("^#CurrentVOffset: (.*)$");
	QRegularExpression CurrentHOffset("^#CurrentHOffset: (.*)$");

	BlockRefresh = true;

	for (int i = 0; i < DataLines.count(); ++i) {
		QString Line = DataLines.at(i);
		if (Line == "") continue;

		QRegularExpressionMatch Match = TableLine.match(Line);
		if (Match.hasMatch()) {
			QString Time = Match.captured(1);
			QString Transition = Match.captured(2);
			QString Pitch = Match.captured(3);
			QString Yaw = Match.captured(4);
			QString Roll = Match.captured(5);
			QString Fov = Match.captured(6);
			QString Eye = Match.captured(7);
			QString Text = Match.captured(8);

			int newrow = ui->Table->rowCount();

			ui->Table->blockSignals(true);
			ui->Table->insertRow(newrow);

			ui->Table->setItem(newrow, TIME_COLUMN, new QTableWidgetItem(Time));
			ui->Table->setItem(newrow, TRANSITION_COLUMN, new QTableWidgetItem(Transition));
			ui->Table->setItem(newrow, PITCH_COLUMN, new QTableWidgetItem(Pitch));
			ui->Table->setItem(newrow, YAW_COLUMN, new QTableWidgetItem(Yaw));
			ui->Table->setItem(newrow, ROLL_COLUMN, new QTableWidgetItem(Roll));
			ui->Table->setItem(newrow, FOV_COLUMN, new QTableWidgetItem(Fov));
			ui->Table->setItem(newrow, EYE_COLUMN, new QTableWidgetItem(Eye));
			ui->Table->setItem(newrow, V_OFFSET_COLUMN, new QTableWidgetItem("0"));
			ui->Table->setItem(newrow, H_OFFSET_COLUMN, new QTableWidgetItem("0"));
			ui->Table->blockSignals(false);
			ui->Table->setItem(newrow, TEXT_COLUMN, new QTableWidgetItem(Text));

			ReadRow(newrow);
			continue;
		}

		Match = OffsetLine.match(Line);
		if (Match.hasMatch()) {
			QString Time = Match.captured(1);
			QString Transition = Match.captured(2);
			QString Pitch = Match.captured(3);
			QString Yaw = Match.captured(4);
			QString Roll = Match.captured(5);
			QString Fov = Match.captured(6);
			QString Eye = Match.captured(7);
			QString V = Match.captured(8);
			QString H = Match.captured(9);
			QString Text = Match.captured(10);

			int newrow = ui->Table->rowCount();

			ui->Table->blockSignals(true);
			ui->Table->insertRow(newrow);

			ui->Table->setItem(newrow, TIME_COLUMN, new QTableWidgetItem(Time));
			ui->Table->setItem(newrow, TRANSITION_COLUMN, new QTableWidgetItem(Transition));
			ui->Table->setItem(newrow, PITCH_COLUMN, new QTableWidgetItem(Pitch));
			ui->Table->setItem(newrow, YAW_COLUMN, new QTableWidgetItem(Yaw));
			ui->Table->setItem(newrow, ROLL_COLUMN, new QTableWidgetItem(Roll));
			ui->Table->setItem(newrow, FOV_COLUMN, new QTableWidgetItem(Fov));
			ui->Table->setItem(newrow, EYE_COLUMN, new QTableWidgetItem(Eye));
			ui->Table->setItem(newrow, V_OFFSET_COLUMN, new QTableWidgetItem(V));
			ui->Table->setItem(newrow, H_OFFSET_COLUMN, new QTableWidgetItem(H));
			ui->Table->blockSignals(false);
			ui->Table->setItem(newrow, TEXT_COLUMN, new QTableWidgetItem(Text));

			ReadRow(newrow);
			continue;
		}

		Match = File.match(Line);
		if (Match.hasMatch()) {
			ui->fileLineEdit->setText(Match.captured(1));
			continue;
		}

		Match = Format.match(Line);
		if (Match.hasMatch()) {
			ui->formatComboBox->setCurrentText(Match.captured(1));
			continue;
		}

		Match = Mode.match(Line);
		if (Match.hasMatch()) {
			ui->modeComboBox->setCurrentText(Match.captured(1));
			continue;
		}

		Match = FOV.match(Line);
		if (Match.hasMatch()) {
			ui->inputFOVSpinBox->setValue(Match.captured(1).toDouble());
			continue;
		}

		Match = HFOV.match(Line);
		if (Match.hasMatch()) {
			ui->HFovSpinBox->setValue(Match.captured(1).toDouble());
			continue;
		}

		Match = VFOV.match(Line);
		if (Match.hasMatch()) {
			ui->VFovSpinBox->setValue(Match.captured(1).toDouble());
			continue;
		}

		Match = FPS.match(Line);
		if (Match.hasMatch()) {
			ui->FPSSpinBox->setValue(Match.captured(1).toDouble());
			continue;
		}
		Match = CMDFile.match(Line);
		if (Match.hasMatch()) {
			ui->CMDFile->setText(Match.captured(1));
			continue;
		}
		Match = VideoLength.match(Line);
		if (Match.hasMatch()) {
			ui->VideoLength->setTime(QTime::fromString(Match.captured(1), ui->RawTime->displayFormat()));
			continue;
		}

		Match = CurrentTime.match(Line);
		if (Match.hasMatch()) {
			ui->RawTime->setTime(QTime::fromString(Match.captured(1), ui->RawTime->displayFormat()));
			continue;
		}

		Match = CurrentPitch.match(Line);
		if (Match.hasMatch()) {
			ui->Pitch->setValue(Match.captured(1).toDouble());
			continue;
		}
		Match = CurrentYaw.match(Line);
		if (Match.hasMatch()) {
			ui->Yaw->setValue(Match.captured(1).toDouble());
			continue;
		}
		Match = CurrentRoll.match(Line);
		if (Match.hasMatch()) {
			ui->Roll->setValue(Match.captured(1).toDouble());
			continue;
		}
		Match = CurrentFOV.match(Line);
		if (Match.hasMatch()) {
			ui->Fov->setValue(Match.captured(1).toDouble());
			continue;
		}
		Match = CurrentEye.match(Line);
		if (Match.hasMatch()) {
			if (Match.captured(1).toInt() > 0)
				ui->RightEye->setChecked(true);
			else
				ui->RightEye->setChecked(false);
			continue;
		}

		Match = ForceMonoMetadata.match(Line);
		if (Match.hasMatch()) {
			if (Match.captured(1).toInt() > 0)
				ui->ForceMonoMetadata->setChecked(true);
			else
				ui->ForceMonoMetadata->setChecked(false);
			continue;
		}

		Match = CurrentVOffset.match(Line);
		if (Match.hasMatch()) {
			ui->VOffset->setValue(Match.captured(1).toDouble());
			continue;
		}

		Match = CurrentHOffset.match(Line);
		if (Match.hasMatch()) {
			ui->HOffset->setValue(Match.captured(1).toDouble());
			continue;
		}

	}

	if (ui->CMDFile->text() == "")
		ui->CMDFile->setText(ui->MovementFile->text() + ".cmd");

	BlockRefresh = false;
	ReloadRawImage(ui->RawTime->time());
}

QString MainWindow::ReadItemValue(int Row, int Column, const QString DefaultValue) {
	QTableWidgetItem *Item;
	Item = ui->Table->item(Row, Column);
	if (Item == nullptr) {
		ui->Table->setItem(Row, Column, new QTableWidgetItem(DefaultValue));
		return DefaultValue;
	} else {
		return Item->text();
	}
}

QTableWidgetItem *MainWindow::ReadItem(int Row, int Column) {
	return ui->Table->item(Row, Column);
}

bool MainWindow::CheckValidTableTime(int Row, QTime &Time) {
	QTableWidgetItem *Item = ReadItem(Row, TIME_COLUMN);
	if (Item == nullptr) {
		Item = new QTableWidgetItem("00:00:00.000");
		Item->setForeground(QBrush(Qt::red));
		ui->Table->setItem(Row, TIME_COLUMN, Item);
		return false;
	}
	QString Value = Item->text();
	Time = QTime::fromString(Value, ui->RawTime->displayFormat());
	if (!Time.isValid()) {
		Item->setForeground(QBrush(Qt::red));
		return false;
	}

	if (Row > 0) {
		QString PrevValue = ReadItemValue(Row - 1, TIME_COLUMN);
		QTime PrevTime = QTime::fromString(PrevValue, ui->RawTime->displayFormat());
		if (Time <= PrevTime) {
			Item->setForeground(QBrush(Qt::red));
			return false;
		}
	}

	Item->setForeground(QBrush());
	return true;
}

void MainWindow::LoadShortcuts() {
	QString ShortcutString;
	Settings->beginGroup("Shortcuts");


#define LOAD_SHORTCUT(name, edit_name, key) ShortcutString = Settings->value(#name, key).toString();\
	ui->edit_name->setKeySequence(QKeySequence(ShortcutString));\
	name = new QShortcut(ui->edit_name->keySequence(), this)

#define LOAD_SHORTCUT_UI(name, edit_name, key) ui->edit_name->setKeySequence(QKeySequence(Settings->value(#name, key).toString()));\
	ui->name->setShortcut(ui->edit_name->keySequence());

	LOAD_SHORTCUT(PitchUP, PitchUpKS, "W");
	LOAD_SHORTCUT(PitchDown, PitchDownKS, "S");
	LOAD_SHORTCUT(YawUP, YawUpKS, "D");
	LOAD_SHORTCUT(YawDown, YawDownKS, "A");
	LOAD_SHORTCUT(RollUP, RollUpKS, "X");
	LOAD_SHORTCUT(RollDown, RollDownKS, "C");
	LOAD_SHORTCUT(FovUP, FovUpKS, "Q");
	LOAD_SHORTCUT(FovDown, FovDownKS, "E");
	LOAD_SHORTCUT(TranUP, TranUpKS, "R");
	LOAD_SHORTCUT(TranDown, TranDownKS, "F");
	LOAD_SHORTCUT(VUP, VUpKS, "Shift+W");
	LOAD_SHORTCUT(VDown, VDownKS, "Shift+S");
	LOAD_SHORTCUT(HUP, HUpKS, "Shift+A");
	LOAD_SHORTCUT(HDown, HDownKS, "Shift+D");
	LOAD_SHORTCUT(FocusText, FocusTextKS, "T");
	LOAD_SHORTCUT(RowUp, RowUpKS, "Shift+Up");
	LOAD_SHORTCUT(RowDown, RowDownKS, "Shift+Down");
	LOAD_SHORTCUT(Undo, UndoKS, "Ctrl+Z");
	LOAD_SHORTCUT(Redo, RedoKS, "Ctrl+Shift+Z");
	LOAD_SHORTCUT(Unselect, UnselectKS, "Tab");

	LOAD_SHORTCUT_UI(Save, SaveKS, "Ctrl+S");
	LOAD_SHORTCUT_UI(AddLine, AddLineKS, "Ins");
	LOAD_SHORTCUT_UI(RemoveLine, RemoveLineKS, "Del");
	LOAD_SHORTCUT_UI(UpdateTime, UpdateTimeKS, "Home");
	LOAD_SHORTCUT_UI(Unselect, UnselectKS, "Esc");
	LOAD_SHORTCUT_UI(NextMinute, NextMinuteKS, "Ctrl+Up");
	LOAD_SHORTCUT_UI(PrevMinute, PrevMinuteKS, "Ctrl+Down");
	LOAD_SHORTCUT_UI(PrevSec, PrevSecondKS, "Left");
	LOAD_SHORTCUT_UI(NextSecond, NextSecondKS, "Right");
	LOAD_SHORTCUT_UI(Prev10Sec, Prev10SecKS, "Down");
	LOAD_SHORTCUT_UI(Next10Sec, Next10SecKS, "Up");
	LOAD_SHORTCUT_UI(PrevFrame, PrevFrameKS, "Ctrl+Left");
	LOAD_SHORTCUT_UI(NextFrame, NextFrameKS, "Ctrl+Right");
	LOAD_SHORTCUT_UI(RightEye, RightEyeKS, "Z");
	LOAD_SHORTCUT_UI(PreviewCurrentLine, PreviewCurrentLineKS, "P");

	Settings->endGroup();
}

void MainWindow::SaveShortcuts() {
	Settings->beginGroup("Shortcuts");
	QKeySequence Key;

#define SAVE_SHORTCUT(name, edit_name, defkey) Key = ui->edit_name->keySequence();\
	if (!Key.isEmpty() && Key.toString() != defkey) Settings->setValue(#name, Key.toString());\
	else Settings->remove(#name);

	SAVE_SHORTCUT(PitchUP, PitchUpKS, "W");
	SAVE_SHORTCUT(PitchDown, PitchDownKS, "S");
	SAVE_SHORTCUT(YawUP, YawUpKS, "D");
	SAVE_SHORTCUT(YawDown, YawDownKS, "A");
	SAVE_SHORTCUT(RollUP, RollUpKS, "X");
	SAVE_SHORTCUT(RollDown, RollDownKS, "C");
	SAVE_SHORTCUT(FovUP, FovUpKS, "Q");
	SAVE_SHORTCUT(FovDown, FovDownKS, "E");
	SAVE_SHORTCUT(TranUP, TranUpKS, "R");
	SAVE_SHORTCUT(TranDown, TranDownKS, "F");
	SAVE_SHORTCUT(VUP, VUpKS, "Shift+W");
	SAVE_SHORTCUT(VDown, VDownKS, "Shift+S");
	SAVE_SHORTCUT(HUP, HUpKS, "Shift+A");
	SAVE_SHORTCUT(HDown, HDownKS, "Shift+D");
	SAVE_SHORTCUT(FocusText, FocusTextKS, "T");
	SAVE_SHORTCUT(RowUp, RowUpKS, "Shift+Up");
	SAVE_SHORTCUT(RowDown, RowDownKS, "Shift+Down");
	SAVE_SHORTCUT(Undo, UndoKS, "Ctrl+Z");
	SAVE_SHORTCUT(Redo, RedoKS, "Ctrl+Shift+Z");
	SAVE_SHORTCUT(Unselect, UnselectKS, "Tab");

	SAVE_SHORTCUT(Save, SaveKS, "Ctrl+S");
	SAVE_SHORTCUT(AddLine, AddLineKS, "Ins");
	SAVE_SHORTCUT(RemoveLine, RemoveLineKS, "Del");
	SAVE_SHORTCUT(UpdateTime, UpdateTimeKS, "Home");
	SAVE_SHORTCUT(Unselect, UnselectKS, "Esc");
	SAVE_SHORTCUT(NextMinute, NextMinuteKS, "Ctrl+Up");
	SAVE_SHORTCUT(PrevMinute, PrevMinuteKS, "Ctrl+Down");
	SAVE_SHORTCUT(PrevSecond, PrevSecondKS, "Left");
	SAVE_SHORTCUT(Prev10Sec, Prev10SecKS, "Down");
	SAVE_SHORTCUT(Next10Sec, Next10SecKS, "Up");
	SAVE_SHORTCUT(PrevFrame, PrevFrameKS, "Ctrl+Left");
	SAVE_SHORTCUT(NextSecond, NextSecondKS, "Right");
	SAVE_SHORTCUT(NextFrame, NextFrameKS, "Ctrl+Right");
	SAVE_SHORTCUT(RightEye, RightEyeKS, "Z");
	SAVE_SHORTCUT(PreviewCurrentLine, PreviewCurrentLineKS, "P");

	Settings->endGroup();

	Settings->sync();
}

void MainWindow::ApplyShortcuts() {

#define APPLY_SHORTCUT(name, edit_name) name->setKey(ui->edit_name->keySequence())
#define APPLY_SHORTCUT_UI(name, edit_name) ui->name->setShortcut(ui->edit_name->keySequence())

	APPLY_SHORTCUT(PitchUP, PitchUpKS);
	APPLY_SHORTCUT(PitchDown, PitchDownKS);
	APPLY_SHORTCUT(YawUP, YawUpKS);
	APPLY_SHORTCUT(YawDown, YawDownKS);
	APPLY_SHORTCUT(RollUP, RollUpKS);
	APPLY_SHORTCUT(RollDown, RollDownKS);
	APPLY_SHORTCUT(FovUP, FovUpKS);
	APPLY_SHORTCUT(FovDown, FovDownKS);
	APPLY_SHORTCUT(TranUP, TranUpKS);
	APPLY_SHORTCUT(TranDown, TranDownKS);
	APPLY_SHORTCUT(VUP, VUpKS);
	APPLY_SHORTCUT(VDown, VDownKS);
	APPLY_SHORTCUT(HUP, HUpKS);
	APPLY_SHORTCUT(HDown, HDownKS);
	APPLY_SHORTCUT(FocusText, FocusTextKS);
	APPLY_SHORTCUT(RowUp, RowUpKS);
	APPLY_SHORTCUT(RowDown, RowDownKS);
	APPLY_SHORTCUT(Undo, UndoKS);
	APPLY_SHORTCUT(Redo, RedoKS);
	APPLY_SHORTCUT(Unselect, UnselectKS);

	APPLY_SHORTCUT_UI(Save, SaveKS);
	APPLY_SHORTCUT_UI(AddLine, AddLineKS);
	APPLY_SHORTCUT_UI(RemoveLine, RemoveLineKS);
	APPLY_SHORTCUT_UI(UpdateTime, UpdateTimeKS);
	APPLY_SHORTCUT_UI(Unselect, UnselectKS);
	APPLY_SHORTCUT_UI(NextMinute, NextMinuteKS);
	APPLY_SHORTCUT_UI(PrevMinute, PrevMinuteKS);
	APPLY_SHORTCUT_UI(PrevSec, PrevSecondKS);
	APPLY_SHORTCUT_UI(NextSecond, NextSecondKS);
	APPLY_SHORTCUT_UI(Prev10Sec, Prev10SecKS);
	APPLY_SHORTCUT_UI(Next10Sec, Next10SecKS);
	APPLY_SHORTCUT_UI(PrevFrame, PrevFrameKS);
	APPLY_SHORTCUT_UI(NextFrame, NextFrameKS);
	APPLY_SHORTCUT_UI(RightEye, RightEyeKS);
	APPLY_SHORTCUT_UI(PreviewCurrentLine, PreviewCurrentLineKS);

}

tableline_t MainWindow::ReadRow(int Row) {
	ui->Table->blockSignals(true);
	tableline_t Out;
	Out.Valid = true;

	QString itemvalue;
	bool ok_conversion;

	QTableWidgetItem *Item = nullptr;

	QTime Time;

	if (CheckValidTableTime(Row, Time)) {
		Out.Time = Time.msecsSinceStartOfDay();
	} else {
		Out.Time = -1;
		Out.Valid = false;
	}

	itemvalue = ReadItemValue(Row, TRANSITION_COLUMN, "0");
	Item = ReadItem(Row, TRANSITION_COLUMN);
	Out.Transition = itemvalue.toDouble(&ok_conversion) * 1000;
	if (!ok_conversion) {
		Item->setForeground(QBrush(Qt::red));
		Out.Transition = 0;
		Out.Valid = false;
	} else {
		Item->setForeground(QBrush());
	}

	if (Row < ui->Table->rowCount() - 1) {
		QString NextTimeValue = ReadItemValue(Row + 1, TIME_COLUMN);
		QTime NextTime = QTime::fromString(NextTimeValue, ui->RawTime->displayFormat());
		QTime TimeTansition = Time.addMSecs(Out.Transition);
		if (Time.msecsSinceStartOfDay() < NextTime.msecsSinceStartOfDay() &&
			TimeTansition.msecsSinceStartOfDay() > NextTime.msecsSinceStartOfDay()) {
			Item->setForeground(QBrush(Qt::red));
			Out.Valid = false;
		} else {
			Item->setForeground(QBrush());
		}
	}

	itemvalue = ReadItemValue(Row, PITCH_COLUMN, "0");
	Item = ReadItem(Row, PITCH_COLUMN);
	Out.Pitch = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		Item->setForeground(QBrush(Qt::red));
		Out.Pitch = 0;
		Out.Valid = false;
	} else {
		Item->setForeground(QBrush());
	}

	itemvalue = ReadItemValue(Row, YAW_COLUMN, "0");
	Item = ReadItem(Row, YAW_COLUMN);
	Out.Yaw = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		Item->setForeground(QBrush(Qt::red));
		Out.Yaw = 0;
		Out.Valid = false;
	} else {
		Item->setForeground(QBrush());
	}

	itemvalue = ReadItemValue(Row, ROLL_COLUMN, "0");
	Item = ReadItem(Row, ROLL_COLUMN);
	Out.Roll = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		Item->setForeground(QBrush(Qt::red));
		Out.Roll = 0;
		Out.Valid = false;
	} else {
		Item->setForeground(QBrush());
	}

	itemvalue = ReadItemValue(Row, FOV_COLUMN, "100");
	Item = ReadItem(Row, FOV_COLUMN);
	Out.FOV = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		Item->setForeground(QBrush(Qt::red));
		Out.FOV = 0;
		Out.Valid = false;
	} else {
		Item->setForeground(QBrush());
	}

	itemvalue = ReadItemValue(Row, EYE_COLUMN, "0");
	if (itemvalue == "0")
		Out.RightEye = false;
	else
		Out.RightEye = true;

	itemvalue = ReadItemValue(Row, V_OFFSET_COLUMN, "0");
	Item = ReadItem(Row, V_OFFSET_COLUMN);
	Out.V = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		Item->setForeground(QBrush(Qt::red));
		Out.V = 0;
		Out.Valid = false;
	} else {
		Item->setForeground(QBrush());
	}

	itemvalue = ReadItemValue(Row, H_OFFSET_COLUMN, "0");
	Item = ReadItem(Row, H_OFFSET_COLUMN);
	Out.H = itemvalue.toDouble(&ok_conversion);
	if (!ok_conversion) {
		Item->setForeground(QBrush(Qt::red));
		Out.H = 0;
		Out.Valid = false;
	} else {
		Item->setForeground(QBrush());
	}

	Out.Text = ReadItemValue(Row, TEXT_COLUMN, "");

	QTableWidgetItem *VertItem = ui->Table->takeVerticalHeaderItem(Row);
	if (VertItem == nullptr) {
		VertItem = new QTableWidgetItem(QString::number(Row + 1));
	}
	if (!Out.Valid) {
		VertItem->setForeground(QBrush(Qt::red));
	} else {
		VertItem->setForeground(QBrush());
	}

	ui->Table->setVerticalHeaderItem(Row, VertItem);

	ui->Table->blockSignals(false);

	return Out;
}

tableline_t MainWindow::LocateRow(const QTime &Time) {
	int Line = -1;
	for (int i = 0; i < ui->Table->rowCount(); ++i) {
		QString RowTimeValue = ReadItemValue(i, TIME_COLUMN);
		QTime RowTime = QTime::fromString(RowTimeValue, ui->RawTime->displayFormat());
		if (Time.msecsSinceStartOfDay() < RowTime.msecsSinceStartOfDay()) break;
		Line = i;
	}
	if (Line >= 0)
		return ReadRow(Line);
	else
		return DefaultTableLine;
}

void MainWindow::on_SaveConfig_clicked() {
	Settings->setValue("ffmpegbin", ui->FfmpegBin->text());
	Settings->setValue("ffprobebin", ui->FfprobeBin->text());
	Settings->setValue("VideoPlayer", ui->VideoPlayer->text());
	Settings->setValue("AutoRun", ui->AutoRun->isChecked());
	Settings->setValue("AutoPlay", ui->AutoPlay->isChecked());
	Settings->setValue("AutoSaveConfig", ui->AutoSaveConfig->isChecked());
	Settings->setValue("AutoSaveMovement", ui->AutoSaveMovement->isChecked());
	Settings->setValue("SaveWindowSettings", ui->SaveWindowSettings->isChecked());
	Settings->setValue("GUIPreviewInterpolation", ui->GUIPreviewInterpolation->currentText());
	Settings->setValue("RAMCache", QString::number(ui->RAMCache->value()));
	Settings->setValue("DiskCache", QString::number(ui->DiskCache->value()));
	Settings->setValue("DiskCacheDir", ui->DiskCacheDir->text());
	Settings->setValue("CacheFormat", ui->CacheFormat->currentText());
	Settings->setValue("PreloadCacheSeoconds", QString::number(ui->PreloadCacheSeoconds->value()));
	Settings->setValue("ShowFramingLines", ui->ShowFramingLines->isChecked());
	Settings->setValue("LimitMovement", ui->LimitMovement->isChecked());

	Settings->setValue("PreviewVideoFormat", ui->PreviewVideoFormat->currentText());
	Settings->setValue("PreviewPreset", ui->PreviewPreset->currentText());
	Settings->setValue("PreviewCRF", QString::number(ui->PreviewCRF->value()));
	Settings->setValue("PreviewAudioFormat", ui->PreviewAudioFormat->currentText());
	Settings->setValue("PreviewAudioBitrate", QString::number(ui->PreviewAudioBitrate->value()));
	Settings->setValue("PreviewCustomFormat", ui->PreviewCustomFormat->text());
	Settings->setValue("PreviewW", QString::number(ui->PreviewW->value()));
	Settings->setValue("PreviewH", QString::number(ui->PreviewH->value()));
	Settings->setValue("PreviewFPS", QString::number(ui->PreviewFPS->value(), 'f', 2));
	Settings->setValue("PreviewInterpolation", ui->PreviewInterpolation->currentText());
	Settings->setValue("PreviewDir", ui->PreviewDir->text());
	Settings->setValue("PreviewTimeBefore", QString::number(ui->PreviewTimeBefore->value()));
	Settings->setValue("PreviewTimeAfter", QString::number(ui->PreviewTimeAfter->value()));

	Settings->setValue("CPreviewVideoFormat", ui->CPreviewVideoFormat->currentText());
	Settings->setValue("CPreviewPreset", ui->CPreviewPreset->currentText());
	Settings->setValue("CPreviewCRF", QString::number(ui->CPreviewCRF->value()));
	Settings->setValue("CPreviewAudioFormat", ui->CPreviewAudioFormat->currentText());
	Settings->setValue("CPreviewAudioBitrate", QString::number(ui->CPreviewAudioBitrate->value()));
	Settings->setValue("CPreviewCustomFormat", ui->CPreviewCustomFormat->text());
	Settings->setValue("CPreviewW", QString::number(ui->CPreviewW->value()));
	Settings->setValue("CPreviewH", QString::number(ui->CPreviewH->value()));
	Settings->setValue("CPreviewFPS", QString::number(ui->CPreviewFPS->value(), 'f', 2));
	Settings->setValue("CPreviewInterpolation", ui->CPreviewInterpolation->currentText());

	Settings->setValue("OutVideoFormat", ui->OutVideoFormat->currentText());
	Settings->setValue("OutPreset", ui->OutPreset->currentText());
	Settings->setValue("OutCRF", QString::number(ui->OutCRF->value()));
	Settings->setValue("OutAudioFormat", ui->OutAudioFormat->currentText());
	Settings->setValue("OutAudioBitrate", QString::number(ui->OutAudioBitrate->value()));
	Settings->setValue("OutW", QString::number(ui->OutW->value()));
	Settings->setValue("OutH", QString::number(ui->OutH->value()));
	Settings->setValue("OutCustomFormat", ui->OutCustomFormat->text());
	Settings->setValue("OutInterpolation", ui->OutInterpolation->currentText());
	Settings->setValue("MovementFile", ui->MovementFile->text());
	//Remove old settings from config file
	if (Settings->contains("FfmpegPreviewOptions")) Settings->remove("FfmpegPreviewOptions");
	if (Settings->contains("FfmpegPreviewFormat")) Settings->remove("FfmpegPreviewFormat");

	Settings->setValue("ScreenshotsDir", ui->ScreenshotsDir->text());

	if (ui->SaveWindowSettings->isChecked()) {
		Settings->beginGroup("MainWindow");
		Settings->setValue("size", size());
		Settings->setValue("pos", pos());
		Settings->setValue("fullscreen", isFullScreen());
		Settings->setValue("LeftTabs", ui->LeftTabs->currentIndex());
		Settings->setValue("RightTabs", ui->RightTabs->currentIndex());
		Settings->setValue("TimeRowSize", ui->Table->columnWidth(TIME_COLUMN));
		Settings->setValue("TransitionRowSize", ui->Table->columnWidth(TRANSITION_COLUMN));
		Settings->setValue("PitchRowSize", ui->Table->columnWidth(PITCH_COLUMN));
		Settings->setValue("YawRowSize", ui->Table->columnWidth(YAW_COLUMN));
		Settings->setValue("RollRowSize", ui->Table->columnWidth(ROLL_COLUMN));
		Settings->setValue("FovRowSize", ui->Table->columnWidth(FOV_COLUMN));
		Settings->setValue("EyeRowSize", ui->Table->columnWidth(EYE_COLUMN));
		Settings->setValue("VOffsetSize", ui->Table->columnWidth(V_OFFSET_COLUMN));
		Settings->setValue("HOffsetSize", ui->Table->columnWidth(H_OFFSET_COLUMN));
		Settings->setValue("EyeRowSize", ui->Table->columnWidth(EYE_COLUMN));
		Settings->setValue("TextRowSize", ui->Table->columnWidth(TEXT_COLUMN));
		Settings->endGroup();
	}

	SaveShortcuts();

}

void MainWindow::ReloadRawImage(const QTime &time) {
	if (BlockRefresh) return;
	if (!FileOk) {
		on_fileLineEdit_textChanged(ui->fileLineEdit->text());
		if (!FileOk) return;
	}
	if (FfmpegRaw.state() != QProcess::NotRunning) {
		FfmpegRawCanceled = true;
		FfmpegRaw.close();
	}

	if (FfmpegFilter.state() != QProcess::NotRunning) {
		FfmpegFilterCanceled = true;
		FfmpegFilter.close();
	}

	RawData.clear();

	CurrentRawTime = time;

	if (Debug) {
		RawStartTime = 0;
		RawReadTime = 0;
		RawPrevirewTime = 0;
		RawTotalTime = 0;
		RawElapsedTimer.start();
	}

	//	TimeCounter.start();

	if (ui->RAMCache->value() > 0) {
		if (RawCache.contains(time)) {
			RawData = *RawCache[time];
			GenerateRawImageTerminate(0, QProcess::NormalExit);
			return;
		}
	}

	if (ui->DiskCache->value() > 0) {

		if (ui->PreloadCacheSeoconds->value() > 0) {
			GenerateDiskCache(1,
							  ui->RawTime->time().addSecs(1),
							  ui->RawTime->time().addSecs(ui->PreloadCacheSeoconds->value()),
							  true);
			OndemandCacheTimer.setSingleShot(true);
			OndemandCacheTimer.start(500);
		}

		QString filename = ui->DiskCacheDir->text() + QDir::separator() + CurrentFilenameHash + "-" +
						   QString::number(time.msecsSinceStartOfDay()) + "." + ui->CacheFormat->currentText().toLower();
		if (QFile::exists(filename)) {
			QFile file(filename);
			if (file.open(QFile::ReadOnly)) {
				RawData = file.readAll();
				GenerateRawImageTerminate(0, QProcess::NormalExit);
			}
			return;
		}
	}

	QStringList params;
	params << "-hide_banner" << "-loglevel" << "error" << "-y" <<
		   "-ss" << QString::number(time.msecsSinceStartOfDay()) + "ms" <<
		   "-i" << ui->fileLineEdit->text() << "-map" << "0:v" << "-vframes" << "1";

	if (ui->CacheFormat->currentText() == "JPG")
		params << "-c:v" << "mjpeg" << "-q:v" << "3";
	else
		params << "-c:v" << ui->CacheFormat->currentText().toLower();

	params << "-f" << "data" << "-";
	FfmpegRawCanceled = false;
	FfmpegRaw.start(ui->FfmpegBin->text(), params);
	FfmpegRaw.waitForStarted(-1);
	if (Debug) RawStartTime = RawElapsedTimer.restart();
}

void MainWindow::ReloadFilterImage() {
	if (BlockRefresh) return;

	if (FfmpegRaw.state() != QProcess::NotRunning) return;

	if (FfmpegFilter.state() != QProcess::NotRunning) {
		NeedFilterUpdate = true;
		return;
	}

	if (RawData.isEmpty()) return;

	FilterResultData.clear();
	QStringList v360_params;

	v360_params.append(ui->formatComboBox->currentText());
	v360_params.append("flat");
	v360_params.append("in_stereo=2d");
	v360_params.append("out_stereo=2d");

	if (ui->inputFOVSpinBox->value() > 0)
		v360_params.append("id_fov=" + QString::number(ui->inputFOVSpinBox->value()));
	if (ui->VFovSpinBox->value() > 0)
		v360_params.append("iv_fov=" + QString::number(ui->VFovSpinBox->value()));
	if (ui->HFovSpinBox->value() > 0)
		v360_params.append("ih_fov=" + QString::number(ui->HFovSpinBox->value()));

	v360_params.append("d_fov=" + QString::number(ui->Fov->value()));
	v360_params.append("pitch=" + QString::number(ui->Pitch->value()));
	v360_params.append("yaw=" + QString::number(ui->Yaw->value()));
	v360_params.append("roll=" + QString::number(ui->Roll->value()));
	if (ui->VOffset->value() != 0)
		v360_params.append("v_offset=" + QString::number(ui->VOffset->value()));
	if (ui->HOffset->value() != 0)
		v360_params.append("h_offset=" + QString::number(ui->HOffset->value()));
	v360_params.append("w=" + QString::number(640));
	v360_params.append("h=" + QString::number(360));
	v360_params.append("interp=" + ui->GUIPreviewInterpolation->currentText());

	v360_params.append("reset_rot=1");

	QString crop = "";

	if (ui->modeComboBox->currentText() == "sbs") {
		if (ui->RightEye->isChecked()) {
			crop = "crop=w=iw/2:h=ih:x=iw/2:y=0,";
		} else {
			crop = "crop=w=iw/2:h=ih:x=0:y=0,";
		}
	} else if (ui->modeComboBox->currentText() == "tb") {
		if (ui->RightEye->isChecked()) {
			crop = "crop=w=iw:h=ih/2:x=0:y=ih/2,";
		} else {
			crop = "crop=w=iw:h=ih/2:x=0:y=0,";
		}
	}

	QStringList params;
	params << "-hide_banner" << "-loglevel" << "error" << "-y" <<
		   "-i" << "-" << "-vf" <<
		   crop + "v360=" + v360_params.join(":")
		   << "-vframes" << "1" << "-c:v" << "bmp" << "-f" << "image2pipe" << "-";
	FfmpegFilterCanceled = false;
	if (Debug) FilterElapsedTimer.start();
	FfmpegFilter.start(ui->FfmpegBin->text(), params);
}

void MainWindow::GenerateFilterImageTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (FfmpegFilterCanceled) {
		FfmpegFilterCanceled = false;
		return;
	}
	if (exitCode != 0) {
		ShowProcessError(&FfmpegFilter);
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;

	if (Debug) FilterReadTime = FilterElapsedTimer.restart();

	FilterPixmap.loadFromData(FilterResultData);
	ScaledFilterPixmap = FilterPixmap.scaledToHeight(ui->CameraFrame->height());
	if (ui->ShowFramingLines->isChecked()) PrintFramingLines();
	ui->CameraFrame->setPixmap(ScaledFilterPixmap);

	if (Debug) {
		FilterPrevirewTime = FilterElapsedTimer.elapsed();
		FilterTotalTime = FilterStartTime + FilterSendTime + FilterReadTime + FilterPrevirewTime;
		ui->DebugLog->appendPlainText("Filter - Start:\t" + QString::number(FilterStartTime) +
									  "\tSend:\t" + QString::number(FilterSendTime) +
									  "\tRead:\t" + QString::number(FilterReadTime) +
									  "\tPreview:\t" + QString::number(FilterPrevirewTime) +
									  "\tTotal:\t" + QString::number(FilterTotalTime));
	}

	if (NeedFilterUpdate) {
		NeedFilterUpdate = false;
		ReloadFilterImage();
	}
}

void MainWindow::ReadRawData() {
	RawData.append(FfmpegRaw.readAllStandardOutput());
}

void MainWindow::ReadFilterData() {
	FilterResultData.append(FfmpegFilter.readAllStandardOutput());
}

void MainWindow::StartFilterProcess() {
	if (Debug) FilterStartTime = FilterElapsedTimer.restart();
	FfmpegFilter.write(RawData);
	FfmpegFilter.closeWriteChannel();
	if (Debug) FilterSendTime = FilterElapsedTimer.restart();
}

void MainWindow::resizeEvent(QResizeEvent */*event*/) {
	if (RawPixmap.isNull()) return;
	ScaledRawPixmap = RawPixmap.scaledToHeight(ui->RawFrame->height());
	ui->RawFrame->setPixmap(ScaledRawPixmap);
	if (FilterPixmap.isNull()) return;
	ScaledFilterPixmap = FilterPixmap.scaledToHeight(ui->CameraFrame->height());
	if (ui->ShowFramingLines->isChecked()) PrintFramingLines();
	ui->CameraFrame->setPixmap(ScaledFilterPixmap);
}

void MainWindow::UpdateTableTime() {
	UpdateItem(TIME_COLUMN, ui->RawTime->text());
}

void MainWindow::UpdateItem(int column, QString value) {
	int selectedrow = GetSelectedRow();
	if (selectedrow >= 0) {
		QTableWidgetItem *CurrentItem = ui->Table->item(selectedrow, column);
		if (CurrentItem != nullptr) {
			if (value == CurrentItem->text()) return;
		}
		QTableWidgetItem *content = new QTableWidgetItem(value);
		QUndoCommand *ChangeItem = new QTableWidgetChange(ui->Table, content, CurrentItem);
		TableUndo->push(ChangeItem);
	}
}

int MainWindow::GetSelectedRow() {
	QItemSelectionModel *model = ui->Table->selectionModel();
	if (model->hasSelection()) {
		QModelIndexList list = model->selectedRows(0);
		return list.at(0).row();
	}
	return -1;
}

QString MainWindow::Row2TSV(int row) {
	QStringList RowText;
	for (int i = 0; i < TOTAL_COLUMNS; ++i) {
		QTableWidgetItem *item = ui->Table->item(row, i);
		if (item == nullptr) {
			if (i == V_OFFSET_COLUMN || i == H_OFFSET_COLUMN) continue;
			RowText.append("");
		} else {
			if ((i == V_OFFSET_COLUMN || i == H_OFFSET_COLUMN) && (item->text() == "0" || item->text() == "")) continue;
			RowText.append(item->text());
		}
	}
	return RowText.join("\t");
}

QStringList MainWindow::Table2TSV() {
	QStringList Output;
	for (int i = 0; i < ui->Table->rowCount(); ++i) {
		Output.append(Row2TSV(i));
	}
	return Output;
}

void MainWindow::GenerateRawImageTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (FfmpegRawCanceled) {
		FfmpegRawCanceled = false;
		return;
	}
	if (exitCode != 0) {
		ShowProcessError(&FfmpegRaw);
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;

	if (Debug) RawReadTime = RawElapsedTimer.restart();

	//	ui->Info->setText("Frame time: " + QString::number(TimeCounter.elapsed()));

	if (ui->RAMCache->value() > 0) {
		if (!RawCache.contains(CurrentRawTime)) {
			RawCache.insert(CurrentRawTime, new QByteArray(RawData), RawData.size());
		}
		ui->CurrentRAMUsage->setText(QString::number(RawCache.totalCost() / 1024 / 1024, 'f', 2));
	}

	if (ui->DiskCache->value() > 0) {
		QString filename = ui->DiskCacheDir->text() + QDir::separator() + CurrentFilenameHash + "-" +
						   QString::number(ui->RawTime->time().msecsSinceStartOfDay()) + "." + ui->CacheFormat->currentText().toLower();
		if (!QFile::exists(filename)) {
			QFile file(filename);
			if (file.open(QFile::WriteOnly)) {
				file.write(RawData);
				file.close();
			}
			RemoveCacheTimer.setSingleShot(true);
			RemoveCacheTimer.start(1000);
		}
	}

	RawPixmap.loadFromData(RawData);
	ScaledRawPixmap = RawPixmap.scaledToHeight(ui->RawFrame->height());
	ui->RawFrame->setPixmap(ScaledRawPixmap);

	if (Debug) {
		RawPrevirewTime = RawElapsedTimer.restart();
		RawTotalTime = RawStartTime + RawReadTime + RawPrevirewTime;
		ui->DebugLog->appendPlainText("Raw - Start:\t" + QString::number(RawStartTime) +
									  "\tRead:\t" + QString::number(RawReadTime) +
									  "\tPreview:\t" + QString::number(RawPrevirewTime) +
									  "\tTotal:\t" + QString::number(RawTotalTime));
	}

	ReloadFilterImage();
}

void MainWindow::FfprobeTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (exitCode != 0) {
		ShowProcessError(&Ffprobe);
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;

	QJsonParseError error;
	VideoInfo = QJsonDocument::fromJson(Ffprobe.readAll(), &error);
	QJsonObject JsonRoot = VideoInfo.object();
	QJsonObject VideoFormat = JsonRoot.value("format").toObject();

	QJsonArray VideoStreams = JsonRoot.value("streams").toArray();
	for (int i = 0; i < VideoStreams.count(); i++) {
		QJsonObject CurrentStream = VideoStreams.at(i).toObject();
		if (CurrentStream.value("codec_type").toString() == "video") {
			QString FrameRate = CurrentStream.value("r_frame_rate").toString();
			QStringList SplitFrameRate = FrameRate.split('/');
			double div1 = SplitFrameRate.first().toInt();
			double div2 = SplitFrameRate.last().toInt();
			ui->FPSSpinBox->setValue(div1 / div2);
			break;
		}
	}

	ui->VideoLength->setTime(QTime::fromMSecsSinceStartOfDay(VideoFormat.value("duration").toString().toDouble() * 1000));
}

void MainWindow::ReadProcessData() {
	QRegularExpression FfmpegProgress(
				"^frame=[[:space:]]*([0-9]+)[[:space:]]+fps=[[:space:]]*([0-9.]+)[[:space:]]+q=[[:space:]]*([0-9.]+)[[:space:]]*size=[[:space:]](.+)[[:space:]]+time=([0-9:.]+)[[:space:]]+bitrate=[[:space:]]*(.*)[[:space:]]+speed=(.*)$");

	QString input = Execution.readAllStandardError();
	QRegularExpressionMatch Match = FfmpegProgress.match(input);

	if (Match.hasMatch()) {
		QTime CurrentTime = QTime::fromString(Match.captured(5).trimmed() + "0", "HH:mm:ss.zzz");
		ui->Progress->setValue(CurrentTime.msecsSinceStartOfDay() / 1000);

		QString Text = "Time: " + Match.captured(5).trimmed();
		Text += ", Speed: " + Match.captured(7).trimmed();
		Text += ", Size: " + Match.captured(4).trimmed();
		Text += ", Bitrate: " + Match.captured(6).trimmed();
		ui->RunState->setText(Text);
		ui->ErrorLog->appendPlainText(input);
	} else {
		ProcessErrorString.append(input);
	}
}

void MainWindow::ProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (exitStatus == QProcess::CrashExit || exitCode != 0) {
		if (ExecutionCanceled) {
			ExecutionCanceled = false;
			if (ProcessIsPreview)
				ui->RunState->setText("Preview process canceled");
			else
				ui->RunState->setText("Process canceled");
		} else {
			ShowProcessError(&Execution, ProcessErrorString);
			if (ProcessIsPreview)
				ui->RunState->setText("Preview process failed");
			else
				ui->RunState->setText("Process failed");
		}
	} else {
		if (ProcessIsPreview) {
			ui->RunState->setText("Preview process terminated");
			if (ui->AutoPlay->isChecked() && ui->VideoPlayer->text() != "")
				QProcess::startDetached(ui->VideoPlayer->text(), QStringList(PreviewFile));
		} else {
			ui->RunState->setText("Process terminated");
		}
	}
	ui->ProgressArea->setVisible(false);
}

void MainWindow::UndoSlot() {
	TableUndo->undo();
	on_Table_itemSelectionChanged();
}

void MainWindow::RedoSlot() {
	TableUndo->redo();
	on_Table_itemSelectionChanged();
}

void MainWindow::UnselectSlot() {
	QWidget *Focused = QApplication::focusWidget();
	if (Focused != nullptr) Focused->clearFocus();
}

void MainWindow::EditTextFocus() {
	ui->Text->setFocus();
}

void MainWindow::ExecuteProcess() {
	if (Execution.state() != QProcess::NotRunning) return;
	ProcessErrorString = "";
	ui->RunState->setText("");
	ui->RunState->setVisible(true);
	ui->Progress->setMaximum(ProcessTotalTime.msecsSinceStartOfDay() / 1000);
	ui->Progress->setValue(0);
	ui->ProgressArea->setVisible(true);
	ExecutionCanceled = false;
	Execution.start(ProcessProgram, ProcessParams);
	Execution.waitForStarted(-1);
}

void MainWindow::on_NextFrame_clicked() {
	QTime newtime = ui->RawTime->time().addMSecs((1000 / ui->FPSSpinBox->value()) + 1);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_PrevFrame_clicked() {
	QTime newtime = ui->RawTime->time().addMSecs((-1000 / ui->FPSSpinBox->value()) - 1);
	if (newtime.msecsSinceStartOfDay() > ui->RawTime->time().msecsSinceStartOfDay()) return;
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_NextSecond_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(1);
	newtime.setHMS(newtime.hour(), newtime.minute(), newtime.second());
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_PrevSec_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(-1);
	if (newtime.msecsSinceStartOfDay() > ui->RawTime->time().msecsSinceStartOfDay()) return;
	newtime.setHMS(newtime.hour(), newtime.minute(), newtime.second());
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_Next10Sec_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(10);
	int seconds = (newtime.second() / 10) * 10;
	newtime.setHMS(newtime.hour(), newtime.minute(), seconds);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_Prev10Sec_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(-10);
	if (newtime.msecsSinceStartOfDay() > ui->RawTime->time().msecsSinceStartOfDay()) return;
	int seconds = (newtime.second() / 10) * 10;
	newtime.setHMS(newtime.hour(), newtime.minute(), seconds);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_NextMinute_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(60);
	newtime.setHMS(newtime.hour(), newtime.minute(), 0);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_PrevMinute_clicked() {
	QTime newtime = ui->RawTime->time().addSecs(-60);
	if (newtime.msecsSinceStartOfDay() > ui->RawTime->time().msecsSinceStartOfDay()) return;
	newtime.setHMS(newtime.hour(), newtime.minute(), 0);
	ui->RawTime->setTime(newtime);
}

void MainWindow::on_SelectFile_clicked() {
	ForceDetectVideo = true;
	QString File = QFileDialog::getOpenFileName(this, "Open Video", "",
				   "Video Files (*.mkv *.mp4 *.avi *.mov *.wmv)");
	if (File != "") ui->fileLineEdit->setText(File);
}

void MainWindow::on_fileLineEdit_textChanged(const QString &arg1) {
	FileOk = false;
	QFileInfo fileinfo(arg1);
	if (fileinfo.exists()) {
		FileOk = true;
		QByteArray file = arg1.toUtf8();
		QByteArray hash = QCryptographicHash::hash(file,
						  QCryptographicHash::Md5).toBase64(QByteArray::Base64UrlEncoding | QByteArray::OmitTrailingEquals);
		CurrentFilename = fileinfo.completeBaseName();
		CurrentFilenameHash = fileinfo.completeBaseName() + QString::fromUtf8(hash);
		CurrentFileDir = fileinfo.path();
		if (ui->FfprobeBin->text() == "") return;
		if (ui->VideoLength->time().msecsSinceStartOfDay() == 0 || ForceDetectVideo) {
			ForceDetectVideo = false;
			QStringList params = {"-i", arg1, "-print_format", "json", "-show_format", "-show_streams"};
			Ffprobe.start(ui->FfprobeBin->text(), params);
			Ffprobe.waitForStarted(-1);
		}
	}
}

void MainWindow::on_Trans_valueChanged(double arg1) {
	UpdateItem(TRANSITION_COLUMN, QString::number(arg1));
}

void MainWindow::on_Pitch_valueChanged(double arg1) {
	UpdateItem(PITCH_COLUMN, QString::number(arg1));
}


void MainWindow::on_Yaw_valueChanged(double arg1) {
	UpdateItem(YAW_COLUMN, QString::number(arg1));
}


void MainWindow::on_Roll_valueChanged(double arg1) {
	UpdateItem(ROLL_COLUMN, QString::number(arg1));
}

void MainWindow::on_Fov_valueChanged(double arg1) {
	UpdateItem(FOV_COLUMN, QString::number(arg1));
}

void MainWindow::on_Text_editingFinished() {
	UpdateItem(TEXT_COLUMN, ui->Text->text());
	ui->Table->setFocus();
}

void MainWindow::on_RightEye_stateChanged(int arg1) {
	if (arg1 == 0 )
		UpdateItem(EYE_COLUMN, "0");
	else
		UpdateItem(EYE_COLUMN, "1");
}

void MainWindow::on_VOffset_valueChanged(double arg1) {
	UpdateItem(V_OFFSET_COLUMN, QString::number(arg1));
}

void MainWindow::on_HOffset_valueChanged(double arg1) {
	UpdateItem(H_OFFSET_COLUMN, QString::number(arg1));
}

void MainWindow::on_AddLine_clicked() {
	if (ui->Table->rowCount() == 0) {
		QUndoCommand *addstart = new QTableWidgetAdd(ui->Table, ui->RawTime, 0,
				QTime::fromMSecsSinceStartOfDay(0), 0, 0, 0, 0, 100, 0, 0, 0, "START");
		TableUndo->push(addstart);
		if (ui->VideoLength->time().msecsSinceStartOfDay() > 0) {
			QUndoCommand *addend = new QTableWidgetAdd(ui->Table, ui->RawTime, 1,
					ui->VideoLength->time(), 0, 0, 0, 0, 100, 0, 0, 0, "END");
			TableUndo->push(addend);
		}
		ui->Table->blockSignals(true);
		ui->Table->selectRow(0);
		ui->Table->blockSignals(false);
	}

	int selectedrow = GetSelectedRow();
	int newrow = selectedrow + 1;

	QUndoCommand *add = new QTableWidgetAdd(ui->Table, ui->RawTime, newrow,
											ui->RawTime->time(),
											1,
											ui->Pitch->value(),
											ui->Yaw->value(),
											ui->Roll->value(),
											ui->Fov->value(),
											ui->RightEye->isChecked(),
											ui->VOffset->value(),
											ui->HOffset->value(),
											"");
	TableUndo->push(add);
	ReadRow(newrow);
	ui->Table->selectRow(newrow);
}

void MainWindow::on_Unselect_clicked() {
	ui->Table->blockSignals(true);
	ui->Table->clearSelection();
	ui->Table->blockSignals(false);
}


void MainWindow::on_RemoveLine_clicked() {
	int selectedrow = GetSelectedRow();
	if (selectedrow != -1 ) {
		QUndoCommand *deletecommand = new QTableWidgetDelete(ui->Table, selectedrow);
		TableUndo->push(deletecommand);
	}
}


void MainWindow::on_UpdateTime_clicked() {
	int selectedrow = GetSelectedRow();
	if (selectedrow == -1 ) return;

	QString value = ui->RawTime->text();
	QTableWidgetItem *CurrentItem = ui->Table->item(selectedrow, TIME_COLUMN);
	if (CurrentItem != nullptr) {
		if (value == CurrentItem->text()) return;
	}
	QTableWidgetItem *content = new QTableWidgetItem(value);
	QUndoCommand *ChangeItem = new QTableWidgetChange(ui->Table, content, CurrentItem);
	TableUndo->push(ChangeItem);
	ReadRow(selectedrow);
}

void MainWindow::on_Save_clicked() {
	if (ui->MovementFile->text() == "") return;

	QString Output;

	Output.append("#File: " + ui->fileLineEdit->text() + "\n");
	Output.append("#Format: " + ui->formatComboBox->currentText() + "\n");
	Output.append("#Mode: " + ui->modeComboBox->currentText() + "\n");
	Output.append("#FOV: " + ui->inputFOVSpinBox->text() + "\n");
	Output.append("#VFOV: " + ui->VFovSpinBox->text() + "\n");
	Output.append("#HFOV: " + ui->HFovSpinBox->text() + "\n");
	Output.append("#FPS: " + QString::number(ui->FPSSpinBox->value(), 'f', 2) + "\n");
	Output.append("#CMDFile: " + ui->CMDFile->text() + "\n");
	Output.append("#VideoLength: " + ui->VideoLength->text() + "\n");
	if (ui->ForceMonoMetadata->isChecked())
		Output.append("#ForceMonoMetadata: 1\n");
	else
		Output.append("#ForceMonoMetadata: 0\n");

	Output.append("#CurrentTime: " + ui->RawTime->text() + "\n");
	Output.append("#CurrentPitch: " + QString::number(ui->Pitch->value()) + "\n");
	Output.append("#CurrentYaw: " + QString::number(ui->Yaw->value()) + "\n");
	Output.append("#CurrentRoll: " + QString::number(ui->Roll->value()) + "\n");
	Output.append("#CurrentFOV: " + QString::number(ui->Fov->value()) + "\n");
	if (ui->RightEye->isChecked())
		Output.append("#CurrentEye: 1\n");
	else
		Output.append("#CurrentEye: 0\n");
	Output.append("#CurrentVOffset: " + QString::number(ui->VOffset->value()) + "\n");
	Output.append("#CurrentHOffset: " + QString::number(ui->HOffset->value()) + "\n");

	Output.append("\n\n");

	QStringList Text = Table2TSV();

	Output.append(Text.join("\n"));

	WriteFile(ui->MovementFile->text(), Output);

}

void MainWindow::TableUpSelect() {
	int selected = GetSelectedRow();
	if (selected < 1) return;
	ui->Table->selectRow(selected - 1);
}

void MainWindow::TableDownSelect() {
	int selected = GetSelectedRow();
	if (selected == ui->Table->rowCount() - 1) return;
	ui->Table->selectRow(selected + 1);
}

void MainWindow::ShowProcessError(QProcess *process, QString CustomErrorLog) {
	QString FailCommand = process->program();
	for (int i = 0; i < process->arguments().count(); ++i) {
		if (process->arguments().at(i).contains(" ")) {
			FailCommand.append(" '" + process->arguments().at(i) + "'");
		} else {
			FailCommand.append(" " + process->arguments().at(i));
		}
	}
	if (!ui->LeftTabs->isTabVisible(LEFT_TAB_ERROR_LOG)) {
		ui->LeftTabs->setTabVisible(LEFT_TAB_ERROR_LOG, true);
		ui->LeftTabs->setCurrentIndex(LEFT_TAB_ERROR_LOG);
	}
	ui->ErrorLog->appendHtml("<p><b>Error in command: " + FailCommand + "</b></p>");
	if (CustomErrorLog != "")
		ui->ErrorLog->appendHtml("<p style=color:red;>" + CustomErrorLog + "</p>");
	else
		ui->ErrorLog->appendHtml("<p style=color:red;>" + process->readAllStandardError() + "</p>");
	ui->ErrorLog->appendHtml("<hr>");
}

void MainWindow::CleanTable() {
	ui->Table->blockSignals(true);
	ui->Table->clearSelection();
	for (int i = ui->Table->rowCount(); i > 0 ; i--) {
		ui->Table->removeRow(i - 1);
	}
	ui->Table->blockSignals(false);
}

void MainWindow::on_Table_itemSelectionChanged() {
	if (!ui->RawFrame->isVisible()) return;
	bool needrawupdate = false;
	bool needfilterupdate = false;
	BlockRefresh = true;

	int selected = GetSelectedRow();

	double trans = ReadItemValue(selected, TRANSITION_COLUMN).toDouble();
	double pitch = ReadItemValue(selected, PITCH_COLUMN).toDouble();
	double yaw = ReadItemValue(selected, YAW_COLUMN).toDouble();
	double roll = ReadItemValue(selected, ROLL_COLUMN).toDouble();
	double fov = ReadItemValue(selected, FOV_COLUMN).toDouble();
	double v =  ReadItemValue(selected, V_OFFSET_COLUMN).toDouble();
	double h =  ReadItemValue(selected, H_OFFSET_COLUMN).toDouble();

	QString text = ReadItemValue(selected, TEXT_COLUMN);

	if (trans != ui->Trans->value()) {
		ui->Trans->setValue(trans);
	}
	if (pitch != ui->Pitch->value()) {
		needfilterupdate = true;
		ui->Pitch->setValue(pitch);
	}
	if (yaw != ui->Yaw->value()) {
		needfilterupdate = true;
		ui->Yaw->setValue(yaw);
	}
	if (roll != ui->Roll->value()) {
		needfilterupdate = true;
		ui->Roll->setValue(roll);
	}
	if (fov != ui->Fov->value()) {
		needfilterupdate = true;
		ui->Fov->setValue(fov);
	}
	if (fov != ui->VOffset->value()) {
		needfilterupdate = true;
		ui->VOffset->setValue(v);
	}
	if (fov != ui->HOffset->value()) {
		needfilterupdate = true;
		ui->HOffset->setValue(h);
	}
	if (text != ui->Text->text()) {
		ui->Text->setText(text);
	}

	bool righteye = (ReadItemValue(selected, EYE_COLUMN) != "0");
	if (righteye != ui->RightEye->isChecked()) {
		needfilterupdate = true;
		ui->RightEye->setChecked(righteye);
	}

	QTime time = QTime::fromString(ReadItemValue(selected, TIME_COLUMN), ui->RawTime->displayFormat());
	if (time != ui->RawTime->time()) {
		needrawupdate = true;
		ui->RawTime->setTime(time);
	}

	BlockRefresh = false;

	if (needrawupdate) {
		ReloadRawImage(ui->RawTime->time());
		return;
	}

	if (needfilterupdate) {
		ReloadFilterImage();
		return;
	}
}

void MainWindow::on_OpenMovementFile_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Open Movement File", "", "Movement Files (*)");
	if (File == "") return;
	ui->MovementFile->setText(File);
	if (!QFile::exists(ui->MovementFile->text())) return;

	LoadTable();

}

void MainWindow::on_NewMovement_clicked() {
	QString File = QFileDialog::getSaveFileName(this, "QFileDialog::getSaveFileName()", "", "All Files (*);");
	if (File == "") return;
	ui->MovementFile->setText(File);
	ui->CMDFile->setText(File + ".cmd");
	TableUndo->clear();
	CleanTable();
	ui->fileLineEdit->setText("");
	ui->VideoLength->setTime(QTime::fromMSecsSinceStartOfDay(0));
}

void MainWindow::on_SaveCMD_clicked() {
	QString fileName = ui->CMDFile->text();
	if (fileName == "") {
		QFileInfo input(ui->MovementFile->text());
		fileName = QFileDialog::getSaveFileName(this, "QFileDialog::getSaveFileName()",
												input.absoluteFilePath() + ".cmd", "All Files (*);;CMD file (*.cmd)");
		if (fileName == "") return;
		ui->CMDFile->setText(fileName);
	}
	if (ui->CMDText->toPlainText() == "")
		on_GenerateCMD_clicked();

	WriteFile(fileName, ui->CMDText->toPlainText());
}

void MainWindow::on_GenerateCMD_clicked() {
	bool AllValid = true;

	QList<tableline_t> Lines;
	for (int i = 0; i < ui->Table->rowCount(); ++i) {
		tableline_t Line = ReadRow(i);
		if (Line.Valid) {
			Lines.append(Line);
		} else {
			AllValid = false;
		}
	}

	if (AllValid) {
		ui->ErrorLines->setVisible(false);
	} else {
		ui->ErrorLines->setVisible(true);
	}

	QString LeftEye;
	QString RightEye;

	if (ui->modeComboBox->currentText() == "sbs") {
		LeftEye = "crop x '0'";
		RightEye = "crop x 'iw/2'";
	}

	if (ui->modeComboBox->currentText() == "tb") {
		LeftEye = "crop y '0'";
		RightEye = "crop y ih/2";
	}

	tableline_t *CurrentLine;
	tableline_t *PrevLine;
	tableline_t *NextLine;
	QString Output;

	for (int i = 0; i < Lines.count(); ++i) {
		QStringList Commands;
		QString TimeIntervar;

		CurrentLine = &Lines[i];

		if (i != 0) {
			PrevLine = &Lines[i - 1];
		} else {
			PrevLine = &DefaultTableLine;
		}

		if ((i + 1) < Lines.count())
			NextLine = &Lines[i + 1];
		else
			break;

		if (CurrentLine->Text != "" && CurrentLine->Text != PrevLine->Text)
			Output.append("#" + CurrentLine->Text + "\n");

		if (CurrentLine->Transition > 0) {
			int TransitionStart = CurrentLine->Time + CurrentLine->Transition;
			TimeIntervar = QString::number(CurrentLine->Time) + "ms-" + QString::number(TransitionStart) + "ms";

			if (CurrentLine->Pitch != PrevLine->Pitch) {
				Commands.append("[expr] v360 pitch 'lerp(" + QString::number(PrevLine->Pitch) + "," +
								QString::number(CurrentLine->Pitch) + ",TI)'");
			}

			if (CurrentLine->Yaw != PrevLine->Yaw) {
				Commands.append("[expr] v360 yaw 'lerp(" + QString::number(PrevLine->Yaw) + "," +
								QString::number(CurrentLine->Yaw) + ",TI)'");
			}

			if (CurrentLine->Roll != PrevLine->Roll) {
				Commands.append("[expr] v360 roll 'lerp(" + QString::number(PrevLine->Roll) + "," +
								QString::number(CurrentLine->Roll) + ",TI)'");
			}

			if (CurrentLine->FOV != PrevLine->FOV) {
				Commands.append("[expr] v360 d_fov 'lerp(" + QString::number(PrevLine->FOV) + "," +
								QString::number(CurrentLine->FOV) + ",TI)'");
			}

			if (CurrentLine->V != PrevLine->V) {
				Commands.append("[expr] v360 v_offset 'lerp(" + QString::number(PrevLine->V) + "," +
								QString::number(CurrentLine->V) + ",TI)'");
			}

			if (CurrentLine->H != PrevLine->H) {
				Commands.append("[expr] v360 h_offset 'lerp(" + QString::number(PrevLine->H) + "," +
								QString::number(CurrentLine->H) + ",TI)'");
			}

			if (Commands.count() > 0)
				Output.append("\t" + TimeIntervar + " " + Commands.join(", ") + ";\n");

			TimeIntervar = QString::number(TransitionStart) + "ms-" + QString::number(NextLine->Time) + "ms";
			Commands.clear();

		} else {
			TimeIntervar = QString::number(CurrentLine->Time) + "ms-" + QString::number(NextLine->Time) + "ms";
		}

		if (CurrentLine->Pitch != PrevLine->Pitch)
			Commands.append("[enter] v360 pitch '" + QString::number(CurrentLine->Pitch) + "'");

		if (CurrentLine->Yaw != PrevLine->Yaw)
			Commands.append("[enter] v360 yaw '" + QString::number(CurrentLine->Yaw) + "'");

		if (CurrentLine->Roll != PrevLine->Roll)
			Commands.append("[enter] v360 roll '" + QString::number(CurrentLine->Roll) + "'");

		if (CurrentLine->FOV != PrevLine->FOV)
			Commands.append("[enter] v360 d_fov '" + QString::number(CurrentLine->FOV) + "'");

		if (CurrentLine->V != PrevLine->V)
			Commands.append("[enter] v360 v_offset '" + QString::number(CurrentLine->V) + "'");

		if (CurrentLine->H != PrevLine->H)
			Commands.append("[enter] v360 h_offset '" + QString::number(CurrentLine->H) + "'");

		if (CurrentLine->RightEye != PrevLine->RightEye) {
			if (CurrentLine->RightEye)
				Commands.append("[enter] " + RightEye);
			else
				Commands.append("[enter] " + LeftEye);
		}

		if (Commands.count() > 0)
			Output.append("\t" + TimeIntervar + " " + Commands.join(", ") + ";\n\n");
	}

	ui->CMDText->setPlainText(Output);

}

QString MainWindow::ComposeFfmpegCommand(
			QString Output,
			QString Template,
			QString VideoCodec,
			int VideoQuality,
			QString VideoPreset,
			QString AudioCodec,
			int AudioQuality,
			int StartMS,
			int EndMS,
			double FPS,
			QString V360Format,
			QString V360Mode,
			double V360IDFOV,
			double V360IVFOV,
			double V360IHFOV,
			double V360DFOV,
			double V360Pitch,
			double V360Yaw,
			double V360Roll,
			double V360VOffset,
			double V360HOffset,
			bool V360RightEye,
			int V360W,
			int V360H,
			QString V360Interp,
			bool V360ResetRot,
			bool ForceMomoMetadata
) {

	QString Fps;
	if (FPS > 0)
		Fps = "fps=fps=" + QString::number(FPS) + ",";

	QStringList v360_params;

	v360_params.append(V360Format);
	v360_params.append("flat");
	v360_params.append("in_stereo=2d");
	v360_params.append("out_stereo=2d");

	if (V360IDFOV > 0)
		v360_params.append("id_fov=" + QString::number(V360IDFOV));
	if (V360IVFOV > 0)
		v360_params.append("iv_fov=" + QString::number(V360IVFOV));
	if (V360IHFOV > 0)
		v360_params.append("ih_fov=" + QString::number(V360IHFOV));

	v360_params.append("d_fov=" + QString::number(V360DFOV));
	v360_params.append("pitch=" + QString::number(V360Pitch));
	v360_params.append("yaw=" + QString::number(V360Yaw));
	v360_params.append("roll=" + QString::number(V360Roll));
	if (V360VOffset != 0)
		v360_params.append("v_offset=" + QString::number(V360VOffset));
	if (V360HOffset != 0)
		v360_params.append("h_offset=" + QString::number(V360HOffset));
	v360_params.append("w=" + QString::number(V360W));
	v360_params.append("h=" + QString::number(V360H));
	v360_params.append("interp=" + V360Interp);
	if (V360ResetRot)
		v360_params.append("reset_rot=1");

	QString Crop = "";

	if (V360Mode == "sbs") {
		if (V360RightEye) {
			Crop = "crop=w=iw/2:h=ih:x=iw/2:y=0,";
		} else {
			Crop = "crop=w=iw/2:h=ih:x=0:y=0,";
		}
	} else if (V360Mode == "tb") {
		if (V360RightEye) {
			Crop = "crop=w=iw:h=ih/2:x=0:y=ih/2,";
		} else {
			Crop = "crop=w=iw:h=ih/2:x=0:y=0,";
		}
	}

	//TODO: try to use setpts=PTS-STARTPTS to restart time

	QString FfmpegGuiOptions = "-hide_banner -loglevel error -stats -y";
	QString RawFilters = Fps + Crop + "v360=" + v360_params.join(":") +
						 ",sendcmd=f='" + ScapeFFFilters(ui->CMDFile->text() + "'");
	QString SimpleFilter = "-vf \"" + RawFilters + "\"";
	QString Start = "";
	if (StartMS > 0 )
		Start = "-ss " + QString::number(StartMS) + "ms";
	QString Input = "-i \"" + ui->fileLineEdit->text() + "\"";
	QString To = "";
	if (EndMS > 0)
		To = "-copyts -to " + QString::number(EndMS) + "ms";

	QString FormatVideo;
	if (VideoCodec == "x265") {
		FormatVideo = "-c:v libx265 -preset " + VideoPreset +
					  " -x265-params log-level=warning -crf " + QString::number(VideoQuality);
	} else if (VideoCodec == "x264") {
		FormatVideo = "-c:v libx264 -preset " + VideoPreset +
					  " -crf " + QString::number(VideoQuality);
	} else if (VideoCodec == "Xvid") {
		FormatVideo = "-c:v libxvid -qscale:v " + QString::number(VideoQuality);
	} else if (VideoCodec == "webp") {
		FormatVideo = "-vcodec libwebp -lossless 0 -compression_level 6 -q:v " + QString::number(VideoQuality);
	}

	QString Metadata;
	if (ForceMomoMetadata) {
		Metadata = "-metadata:s:v stereo_mode=mono";
	}

	QString FormatAudio;
	if (AudioCodec == "Copy") {
		FormatAudio = " -c:a copy";
	} else if (AudioCodec == "MP3") {
		FormatAudio = "-c:a libmp3lame -b:a " + QString::number(AudioQuality);
	} else if (AudioCodec == "AAC") {
		FormatAudio = "-c:a aac -b:a " + QString::number(AudioQuality);
	} else if (AudioCodec == "Vorbis") {
		FormatAudio = "-c:a libvorbis -b:a " + QString::number(AudioQuality);
	} else if (AudioCodec == "Opus") {
		FormatAudio = "-c:a libopus -b:a " + QString::number(AudioQuality);
	}

	QString Format = FormatVideo + " " + FormatAudio;
	if (Metadata != "") Format += " " + Metadata;

	QString Result = Template;

	QFileInfo OutFileInfo(Output);

	if (Result.contains("%FFMPEG%")) Result.replace("%FFMPEG%", ui->FfmpegBin->text());
	if (Result.contains("%FFMPEG_GUI_OPTIONS%")) Result.replace("%FFMPEG_GUI_OPTIONS%", FfmpegGuiOptions);
	if (Result.contains("%START_TIME%")) Result.replace("%START_TIME%", Start);
	if (Result.contains("%INPUT%")) Result.replace("%INPUT%", Input);
	if (Result.contains("%TO_TIME%")) Result.replace("%TO_TIME%", To);
	if (Result.contains("%SIMPLE_FILTER%")) Result.replace("%SIMPLE_FILTER%", SimpleFilter);
	if (Result.contains("%RAW_FILTER%")) Result.replace("%RAW_FILTER%", RawFilters);
	if (Result.contains("%FORMAT%")) Result.replace("%FORMAT%", Format);
	if (Result.contains("%VIDEO_FORMAT%")) Result.replace("%VIDEO_FORMAT%", FormatVideo);
	if (Result.contains("%AUDIO_FORMAT%")) Result.replace("%AUDIO_FORMAT%", FormatAudio);
	if (Result.contains("%VIDEO_QUALITY%")) Result.replace("%VIDEO_QUALITY%", QString::number(VideoQuality));
	if (Result.contains("%VIDEO_PRESET%")) Result.replace("%VIDEO_PRESET%", VideoPreset);
	if (Result.contains("%VIDEO_CODEC%")) Result.replace("%VIDEO_CODEC%", VideoCodec);
	if (Result.contains("%AUDIO_QUALITY%")) Result.replace("%AUDIO_QUALITY%", QString::number(AudioQuality));
	if (Result.contains("%AUDIO_CODEC%")) Result.replace("%AUDIO_CODEC%", AudioCodec);
	if (Result.contains("%METADATA%")) Result.replace("%METADATA%", Metadata);
	if (Result.contains("%OUTPUT%")) Result.replace("%OUTPUT%", "\"" + Output + "\"");
	if (Result.contains("%OUT_FILE_NAME%")) Result.replace("%OUT_FILE_NAME%",
				OutFileInfo.path() + QDir::separator() + OutFileInfo.baseName());

	return Result;
}

QString MainWindow::GetTemplate(QString Format) {
	if (Format == "Custom") return "";
	if (Format == "gif") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %START_TIME% %INPUT% %TO_TIME% -filter_complex \"[0:v] %RAW_FILTER%,split [a][b]; [a] palettegen=stats_mode=single [p]; [b][p] paletteuse=new=1\" %OUTPUT%";
	}
	if (Format == "NVENC H264") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% -hwaccel_output_format cuda %START_TIME% %INPUT% %TO_TIME% %SIMPLE_FILTER% -c:v h264_nvenc -preset %VIDEO_PRESET% -cq %VIDEO_QUALITY% -profile:v high %AUDIO_FORMAT% %METADATA% %OUTPUT%";
	}
	if (Format == "NVENC H265") {
		return "%FFMPEG% %FFMPEG_GUI_OPTIONS% -hwaccel_output_format cuda %START_TIME% %INPUT% %TO_TIME% %SIMPLE_FILTER% -c:v hevc_nvenc -preset %VIDEO_PRESET% -cq %VIDEO_QUALITY% %AUDIO_FORMAT% %METADATA% %OUTPUT%";
	}

	return "%FFMPEG% %FFMPEG_GUI_OPTIONS% %START_TIME% %INPUT% %TO_TIME% %SIMPLE_FILTER% %FORMAT% %OUTPUT%";
}

QString MainWindow::GetExtension(QString Format) {
	QString Result = ".mkv";
	if (Format == "gif") Result = ".gif";
	if (Format == "webp") Result = ".webp";
	return Result;
}

void MainWindow::DisableRunButtons() {
	ui->ExecutePreviewCommand->setEnabled(false);
	ui->CPreviewRun->setEnabled(false);
	ui->ExecuteOutCommand->setEnabled(false);
}

void MainWindow::GenerateDiskCache(int seconds) {
	GenerateDiskCache(seconds, QTime::fromMSecsSinceStartOfDay(0), ui->VideoLength->time(), false);
}

void MainWindow::GenerateDiskCache(int seconds, QTime start, QTime end, bool ondemand) {
	if (ui->DiskCache->value() == 0) return;
	if (ui->DiskCacheDir->text() == "") return;
	if (end.msecsSinceStartOfDay() == 0) return;

	QDir dir;

	if (!dir.exists(ui->DiskCacheDir->text())) {
		if (!dir.mkpath(ui->DiskCacheDir->text())) return;
	}

	QTime Current;
	Current.setHMS(start.hour(), start.minute(), start.second());
	while (Current.msecsSinceStartOfDay() < end.msecsSinceStartOfDay()) {
		if (Current.msecsSinceStartOfDay() >= ui->VideoLength->time().msecsSinceStartOfDay()) return;
		if (ondemand)
			OndemandDiskCacheQueue.insert(Current, 0);
		else
			DiskCacheQueue.enqueue(Current);
		Current = Current.addSecs(seconds);
	}
}

qint64 MainWindow::DiskCacheUsage() {
	qint64 total = 0;
	qint64 max = ui->DiskCache->value() * 1024 * 1024;
	QDirIterator it(ui->DiskCacheDir->text());
	while (it.hasNext()) {
		it.next();
		total += it.fileInfo().size();
	}

	if (total > max) {
		CleanDiskCache(total - max);
	}

	return total;
}

void MainWindow::TakeScreenshot(QTime Time, int V360W, int V360H, int FinalW, int FinalH) {
	if (ui->ScreenshotsDir->text() == "") return;

	QString V360Resolution = QString::number(V360W) + "x" + QString::number(V360H);
	QString ScreenshotFilename = ui->ScreenshotsDir->text() + QDir::separator() + CurrentFilename + "-" +
								 QString::number(Time.msecsSinceStartOfDay()) + "-" + V360Resolution;
	if (FinalW != 0 || FinalH != 0) ScreenshotFilename += "-Rescaled";
	ScreenshotFilename += "." + ui->ScreenshotsFormat->currentText();

	//TODO: Convert this to common function
	QStringList v360_params;

	v360_params.append(ui->formatComboBox->currentText());
	v360_params.append("flat");
	v360_params.append("in_stereo=2d");
	v360_params.append("out_stereo=2d");

	if (ui->inputFOVSpinBox->value() > 0)
		v360_params.append("id_fov=" + QString::number(ui->inputFOVSpinBox->value()));
	if (ui->VFovSpinBox->value() > 0)
		v360_params.append("iv_fov=" + QString::number(ui->VFovSpinBox->value()));
	if (ui->HFovSpinBox->value() > 0)
		v360_params.append("ih_fov=" + QString::number(ui->HFovSpinBox->value()));

	v360_params.append("d_fov=" + QString::number(ui->Fov->value()));
	v360_params.append("pitch=" + QString::number(ui->Pitch->value()));
	v360_params.append("yaw=" + QString::number(ui->Yaw->value()));
	v360_params.append("roll=" + QString::number(ui->Roll->value()));
	if (ui->VOffset->value() != 0)
		v360_params.append("v_offset=" + QString::number(ui->VOffset->value()));
	if (ui->HOffset->value() != 0)
		v360_params.append("h_offset=" + QString::number(ui->HOffset->value()));
	v360_params.append("w=" + QString::number(V360W));
	v360_params.append("h=" + QString::number(V360H));
	v360_params.append("interp=lanczos");

	QString crop = "";

	if (ui->modeComboBox->currentText() == "sbs") {
		if (ui->RightEye->isChecked()) {
			crop = "crop=w=iw/2:h=ih:x=iw/2:y=0";
		} else {
			crop = "crop=w=iw/2:h=ih:x=0:y=0";
		}
	} else if (ui->modeComboBox->currentText() == "tb") {
		if (ui->RightEye->isChecked()) {
			crop = "crop=w=iw:h=ih/2:x=0:y=ih/2";
		} else {
			crop = "crop=w=iw:h=ih/2:x=0:y=0";
		}
	}

	QStringList Filters;
	if (crop != "") Filters.append(crop);
	Filters.append("v360=" + v360_params.join(":"));
	if (FinalW != 0 || FinalH != 0) {
		QString Scale = "scale=";
		if (FinalW == 0)
			Scale += "-2";
		else
			Scale += QString::number(FinalW);
		Scale += ":";
		if (FinalH == 0)
			Scale += "-2";
		else
			Scale += QString::number(FinalH);
		Scale += ":flags=lanczos";
		Filters.append(Scale);
	}

	QStringList params;
	params << "-hide_banner" << "-loglevel" << "error" << "-y" <<
		   "-ss" << QString::number(Time.msecsSinceStartOfDay()) + "ms" <<
		   "-i" << ui->fileLineEdit->text() <<
		   "-vf" << Filters.join(",")
		   << "-vframes" << "1";

	if (ui->ScreenshotsFormat->currentText() == "jpg") {
		params << "-q:v" << QString::number(ui->ScreenshotsQuality->value());
	}
	if (ui->ScreenshotsFormat->currentText() == "webp") {
		params << "-c:v" << "libwebp" << "-q:v" << QString::number(ui->ScreenshotsQuality->value()) << "-compression_level" <<
			   "6";
	}

	params << ScreenshotFilename;

	EnableScreenshotsButtons(false);
	ui->ScreenshotInfo->setText("Processing...");

	ScreenshotProcess.start(ui->FfmpegBin->text(), params);
	ScreenshotProcess.waitForStarted(-1);
}

void MainWindow::EnableScreenshotsButtons(bool Enable) {
	ui->TakeScreenshot->setEnabled(Enable);
	ui->TakeScreenshot1280->setEnabled(Enable);
	ui->TakeScreenshot1920->setEnabled(Enable);
	ui->TakeScreenshotRescaled->setEnabled(Enable);
}

void MainWindow::PrintFramingLines() {
	int width = ScaledFilterPixmap.width();
	int height = ScaledFilterPixmap.height();

	int halfw = width / 2;
	int halfh = height / 2;

	int thirdw = width / 3;
	int thirdh = height / 3;

	int sixthw = width / 6;
	int sixthh = height / 6;

	int twelfthw = width / 12;
	int twelfthh = height / 12;


	QPainter paint(&ScaledFilterPixmap);
	QPen pen(Qt::white);
	pen.setWidth(2);
	paint.setPen(pen);
	paint.drawLine(0,
				   halfh,
				   sixthw,
				   halfh);

	paint.drawLine(width,
				   halfh,
				   width - sixthw,
				   halfh);

	paint.drawLine(halfw,
				   0,
				   halfw,
				   sixthh);

	paint.drawLine(halfw,
				   height,
				   halfw,
				   height - sixthh);


	paint.drawLine(thirdw,
				   thirdh,
				   thirdw + twelfthw,
				   thirdh);

	paint.drawLine(thirdw,
				   thirdh,
				   thirdw,
				   thirdh + twelfthh);

	paint.drawLine(width - thirdw,
				   thirdh,
				   width - thirdw - twelfthw,
				   thirdh);

	paint.drawLine(width - thirdw,
				   thirdh,
				   width - thirdw,
				   thirdh + twelfthh);

	paint.drawLine(thirdw,
				   height - thirdh,
				   thirdw + twelfthw,
				   height - thirdh);

	paint.drawLine(thirdw,
				   height - thirdh,
				   thirdw,
				   height - thirdh - twelfthh);

	paint.drawLine(width - thirdw,
				   height - thirdh,
				   width - thirdw - twelfthw,
				   height - thirdh);

	paint.drawLine(width - thirdw,
				   height - thirdh,
				   width - thirdw,
				   height - thirdh - twelfthh);

	pen.setWidth(1);
	pen.setColor(Qt::black);
	paint.setPen(pen);

	paint.drawLine(0,
				   halfh,
				   sixthw,
				   halfh);

	paint.drawLine(width,
				   halfh,
				   width - sixthw,
				   halfh);

	paint.drawLine(halfw,
				   0,
				   halfw,
				   sixthh);

	paint.drawLine(halfw,
				   height,
				   halfw,
				   height - sixthh);

	paint.drawLine(thirdw,
				   thirdh,
				   thirdw + twelfthw,
				   thirdh);

	paint.drawLine(thirdw,
				   thirdh,
				   thirdw,
				   thirdh + twelfthh);

	paint.drawLine(width - thirdw,
				   thirdh,
				   width - thirdw - twelfthw,
				   thirdh);

	paint.drawLine(width - thirdw,
				   thirdh,
				   width - thirdw,
				   thirdh + twelfthh);

	paint.drawLine(thirdw,
				   height - thirdh,
				   thirdw + twelfthw,
				   height - thirdh);

	paint.drawLine(thirdw,
				   height - thirdh,
				   thirdw,
				   height - thirdh - twelfthh);

	paint.drawLine(width - thirdw,
				   height - thirdh,
				   width - thirdw - twelfthw,
				   height - thirdh);

	paint.drawLine(width - thirdw,
				   height - thirdh,
				   width - thirdw,
				   height - thirdh - twelfthh);

}

void MainWindow::CleanDiskCache(qint64 size) {
	QDir Dir(ui->DiskCacheDir->text());
	QFileInfoList List = Dir.entryInfoList(QDir::Files | QDir::NoDot | QDir::NoDotDot, QDir::Time | QDir::Reversed);
	qint64 DeletedSize = 0;
	for (int i = 0; i < List.count(); ++i) {
		if (size > 0 && DeletedSize >= size) return;
		QFileInfo file = List.value(i);
		DeletedSize += file.size();
		QFile::remove(file.filePath());
	}
}

void MainWindow::DiskCacheStart() {
	DiskCacheProcessTerminate(0, QProcess::NormalExit);
}

void MainWindow::RemoveCacheTimeout() {
	ui->CurrentDiskUsage->setText(QString::number((double)DiskCacheUsage() / 1024 / 1024, 'f', 2));
}

void MainWindow::ScreenshotProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (exitCode != 0) {
		ShowProcessError(&ScreenshotProcess);
		ui->ScreenshotInfo->setText("Failed taking screenshot");
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;
	if (ScreenshotProcess.state() != QProcess::NotRunning) return;

	ui->ScreenshotInfo->setText("Done");
	EnableScreenshotsButtons(true);
}

void MainWindow::PitchUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Pitch->stepUp();
}

void MainWindow::PitchDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Pitch->stepDown();
}

void MainWindow::YawUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Yaw->stepUp();
}

void MainWindow::YawDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Yaw->stepDown();
}

void MainWindow::RollUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Roll->stepUp();
}

void MainWindow::RollDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Roll->stepDown();
}

void MainWindow::FovUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Fov->stepUp();
}

void MainWindow::FovDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->Fov->stepDown();
}

void MainWindow::VUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->VOffset->stepUp();
}

void MainWindow::VDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->VOffset->stepDown();
}

void MainWindow::HUPEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->HOffset->stepUp();
}

void MainWindow::HDownEvent() {
	if (ui->LimitMovement->isChecked() && FfmpegFilter.state() != QProcess::NotRunning) return;
	ui->HOffset->stepDown();
}

void MainWindow::DiskCacheProcessTerminate(int exitCode, QProcess::ExitStatus exitStatus) {
	if (ui->DiskCacheDir->text() == "") return;
	if (exitCode != 0) {
		ShowProcessError(&DiskCacheProcess);
		return;
	}
	if (exitStatus == QProcess::CrashExit) return;
	if (DiskCacheProcess.state() != QProcess::NotRunning) return;

	if (!FileOk) {
		on_fileLineEdit_textChanged(ui->fileLineEdit->text());
		if (!FileOk) return;
	}

	QTime time;
	QString file;

	do {
		if (!OndemandDiskCacheQueue.isEmpty()) {
			time = OndemandDiskCacheQueue.firstKey();
			OndemandDiskCacheQueue.remove(time);
			if (ui->RawTime->time() > time) continue;
		} else {
			if (DiskCacheQueue.isEmpty()) return;
			time = DiskCacheQueue.dequeue();
		}
		if (time >= ui->VideoLength->time()) continue;

		file = ui->DiskCacheDir->text() + QDir::separator() + CurrentFilenameHash + "-" + QString::number(
						   time.msecsSinceStartOfDay()) + "." +
			   ui->CacheFormat->currentText().toLower();
		file = QDir::toNativeSeparators(file);
	} while (file == "" || QFile::exists(file));

	QStringList params;
	params << "-hide_banner" << "-loglevel" << "error" << "-y" <<
		   "-ss" << QString::number(time.msecsSinceStartOfDay()) + "ms" <<
		   "-i" << ui->fileLineEdit->text() << "-map" << "0:v" << "-vframes" << "1";
	if (ui->CacheFormat->currentText() == "JPG") params << "-q:v" << "3";
	params << file;

	DiskCacheProcess.start(ui->FfmpegBin->text(), params);
	DiskCacheProcess.waitForStarted(-1);
}

void MainWindow::on_PreviewTransition_clicked() {
	tableline_t Line = DefaultTableLine;
	int selected = GetSelectedRow();
	if (selected == -1) return;
	Line = ReadRow(selected);
	if (!Line.Valid) return;

	if (ui->CMDFile->text() == "") return;

	on_GenerateCMD_clicked();
	on_SaveCMD_clicked();

	tableline_t AntLine = DefaultTableLine;
	AntLine = ReadRow(selected - 1);

	QString Ext = GetExtension(ui->PreviewVideoFormat->currentText());

	PreviewFile = ui->PreviewDir->text();
	if (PreviewFile == "") PreviewFile = CurrentFileDir;
	PreviewFile += QDir::separator() + CurrentFilename + "_Transition_";
	if (Line.Text == "")
		PreviewFile += QString::number(Line.Time) + Ext;
	else
		PreviewFile += Line.Text + Ext;

	PreviewFile = QDir::toNativeSeparators(PreviewFile);

	QString Template = GetTemplate(ui->PreviewVideoFormat->currentText());
	if (Template == "") Template = ui->PreviewCustomFormat->text();

	QString FfmpegCommand = ComposeFfmpegCommand(
										PreviewFile,
										Template,
										ui->PreviewVideoFormat->currentText(),
										ui->PreviewCRF->value(),
										ui->PreviewPreset->currentText(),
										ui->PreviewAudioFormat->currentText(),
										ui->PreviewAudioBitrate->value(),
										Line.Time - ui->PreviewTimeBefore->value(),
										(int)(Line.Time + Line.Transition + ui->PreviewTimeAfter->value()),
										ui->PreviewFPS->value(),
										ui->formatComboBox->currentText(),
										ui->modeComboBox->currentText(),
										ui->inputFOVSpinBox->value(),
										ui->VFovSpinBox->value(),
										ui->HFovSpinBox->value(),
										AntLine.FOV,
										AntLine.Pitch,
										AntLine.Yaw,
										AntLine.Roll,
										AntLine.V,
										AntLine.H,
										AntLine.RightEye,
										ui->PreviewW->value(),
										ui->PreviewH->value(),
										ui->PreviewInterpolation->currentText(),
										true,
										ui->ForceMonoMetadata->isChecked()
							);

	if (FfmpegCommand == "") return;

	DisableRunButtons();

	ProcessParams = QProcess::splitCommand(FfmpegCommand);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	ProcessTotalTime = QTime::fromMSecsSinceStartOfDay(Line.Transition + 4000);

	ProcessIsPreview = true;

	ui->ExecutePreviewCommand->setEnabled(true);

	ui->PreviewCommand->setPlainText(FfmpegCommand);
	if (ui->AutoRun->isChecked()) ExecuteProcess();

}

void MainWindow::on_CPreviewGenerateFfmpeg_clicked() {
	if (ui->CMDFile->text() == "") return;

	on_GenerateCMD_clicked();
	on_SaveCMD_clicked();

	QString Ext = GetExtension(ui->CPreviewVideoFormat->currentText());

	QString OutFilename = ui->CPreviewFile->text();

	if (OutFilename == "") {
		OutFilename = ui->fileLineEdit->text();
		OutFilename += "_Complete_preview" + Ext;
		ui->CPreviewFile->setText(OutFilename);
	}

	QString Template = GetTemplate(ui->CPreviewVideoFormat->currentText());
	if (Template == "") Template = ui->CPreviewCustomFormat->text();

	tableline_t FirstLine = ReadRow(0);
	if (FirstLine.Time != 0) FirstLine = DefaultTableLine;

	QString FfmpegCommand = ComposeFfmpegCommand(
										OutFilename,
										Template,
										ui->CPreviewVideoFormat->currentText(),
										ui->CPreviewCRF->value(),
										ui->CPreviewPreset->currentText(),
										ui->CPreviewAudioFormat->currentText(),
										ui->CPreviewAudioBitrate->value(),
										0,
										0,
										ui->CPreviewFPS->value(),
										ui->formatComboBox->currentText(),
										ui->modeComboBox->currentText(),
										ui->inputFOVSpinBox->value(),
										ui->VFovSpinBox->value(),
										ui->HFovSpinBox->value(),
										FirstLine.FOV,
										FirstLine.Pitch,
										FirstLine.Yaw,
										FirstLine.Roll,
										FirstLine.V,
										FirstLine.H,
										FirstLine.RightEye,
										ui->CPreviewW->value(),
										ui->CPreviewH->value(),
										ui->CPreviewInterpolation->currentText(),
										true,
										ui->ForceMonoMetadata->isChecked()
							);

	if (FfmpegCommand == "") return;

	DisableRunButtons();

	ProcessParams = QProcess::splitCommand(FfmpegCommand);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	ProcessTotalTime = ui->VideoLength->time();

	ui->CPreviewRun->setEnabled(true);

	ui->CPreviewCommand->setPlainText(FfmpegCommand);
	ProcessIsPreview = true;
	if (ui->AutoRun->isChecked()) ExecuteProcess();
}

void MainWindow::on_PreviewFragment_clicked() {
	if (ui->CMDFile->text() == "") return;

	on_GenerateCMD_clicked();
	on_SaveCMD_clicked();

	QString Ext = GetExtension(ui->PreviewVideoFormat->currentText());

	PreviewFile = ui->PreviewDir->text();
	if (PreviewFile == "") PreviewFile = CurrentFileDir;
	PreviewFile += QDir::separator() + CurrentFilename + "_PrevieFragment" +
				   QString::number(ui->PreviewStart->time().msecsSinceStartOfDay()) + "-" +
				   QString::number(ui->PreviewEnd->time().msecsSinceStartOfDay()) + Ext;
	PreviewFile = QDir::toNativeSeparators(PreviewFile);

	QString Template = GetTemplate(ui->PreviewVideoFormat->currentText());
	if (Template == "") Template = ui->PreviewCustomFormat->text();

	tableline_t StartLine = LocateRow(ui->PreviewStart->time());

	if (!StartLine.Valid) {
		StartLine.FOV = 110;
		StartLine.Pitch = 0;
		StartLine.Yaw = 0;
		StartLine.Roll = 0;
		StartLine.RightEye = false;
	}

	QString FfmpegCommand = ComposeFfmpegCommand(
										PreviewFile,
										Template,
										ui->PreviewVideoFormat->currentText(),
										ui->PreviewCRF->value(),
										ui->PreviewPreset->currentText(),
										ui->PreviewAudioFormat->currentText(),
										ui->PreviewAudioBitrate->value(),
										ui->PreviewStart->time().msecsSinceStartOfDay(),
										ui->PreviewEnd->time().msecsSinceStartOfDay(),
										ui->PreviewFPS->value(),
										ui->formatComboBox->currentText(),
										ui->modeComboBox->currentText(),
										ui->inputFOVSpinBox->value(),
										ui->VFovSpinBox->value(),
										ui->HFovSpinBox->value(),
										StartLine.FOV,
										StartLine.Pitch,
										StartLine.Yaw,
										StartLine.Roll,
										StartLine.V,
										StartLine.H,
										StartLine.RightEye,
										ui->PreviewW->value(),
										ui->PreviewH->value(),
										ui->PreviewInterpolation->currentText(),
										true,
										ui->ForceMonoMetadata->isChecked()
							);

	if (FfmpegCommand == "") return;

	DisableRunButtons();

	ProcessParams = QProcess::splitCommand(FfmpegCommand);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	ProcessTotalTime = QTime::fromMSecsSinceStartOfDay(ui->PreviewEnd->time().msecsSinceStartOfDay() -
					   ui->PreviewStart->time().msecsSinceStartOfDay());

	ui->ExecutePreviewCommand->setEnabled(true);

	ui->PreviewCommand->setPlainText(FfmpegCommand);
	ProcessIsPreview = true;
	if (ui->AutoRun->isChecked()) ExecuteProcess();
}

void MainWindow::on_SelectOutFile_clicked() {
	QString fileName = QFileDialog::getSaveFileName(this, "QFileDialog::getSaveFileName()",
					   ui->fileLineEdit->text() + "_reencode2normal.mkv", "MKV Files (*.mkv)");
	if (fileName != "") ui->OutFile->setText(fileName);
}

void MainWindow::on_OutGenerateCommand_clicked() {
	if (ui->CMDFile->text() == "" || !QFile::exists(ui->CMDFile->text())) return;

	on_GenerateCMD_clicked();
	on_SaveCMD_clicked();

	QString Ext = GetExtension(ui->OutVideoFormat->currentText());

	if (ui->OutFile->text() == "") {
		ui->OutFile->setText(ui->fileLineEdit->text() + "_reencode2normal" + Ext);
	}

	QString Template = GetTemplate(ui->OutVideoFormat->currentText());
	if (Template == "") Template = ui->OutCustomFormat->text();

	tableline_t FirstLine = ReadRow(0);
	if (FirstLine.Time != 0) FirstLine = DefaultTableLine;

	QString FfmpegCommand = ComposeFfmpegCommand(
										ui->OutFile->text(),
										Template,
										ui->OutVideoFormat->currentText(),
										ui->OutCRF->value(),
										ui->OutPreset->currentText(),
										ui->OutAudioFormat->currentText(),
										ui->OutAudioBitrate->value(),
										0,
										0,
										0,
										ui->formatComboBox->currentText(),
										ui->modeComboBox->currentText(),
										ui->inputFOVSpinBox->value(),
										ui->VFovSpinBox->value(),
										ui->HFovSpinBox->value(),
										FirstLine.FOV,
										FirstLine.Pitch,
										FirstLine.Yaw,
										FirstLine.Roll,
										FirstLine.V,
										FirstLine.H,
										FirstLine.RightEye,
										ui->OutW->value(),
										ui->OutH->value(),
										ui->OutInterpolation->currentText(),
										true,
										ui->ForceMonoMetadata->isChecked()
							);

	if (FfmpegCommand == "") return;

	DisableRunButtons();

	ProcessParams = QProcess::splitCommand(FfmpegCommand);
	if (ProcessParams.count() > 1)
		ProcessProgram = ProcessParams.takeFirst();

	ProcessTotalTime = ui->VideoLength->time();
	ProcessIsPreview = false;

	ui->ExecuteOutCommand->setEnabled(true);

	ui->OutCommand->setPlainText(FfmpegCommand);
}

void MainWindow::on_SelectPreviewFile_clicked() {
	QString fileName = QFileDialog::getSaveFileName(this, "QFileDialog::getSaveFileName()",
					   ui->fileLineEdit->text() + "_Complete_preview.mkv", "MKV Files (*.mkv)");
	if (fileName != "") ui->CPreviewFile->setText(fileName);
}

void MainWindow::on_formatComboBox_currentTextChanged(const QString &/*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImage();
}

void MainWindow::on_modeComboBox_currentTextChanged(const QString &/*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImage();
}

void MainWindow::on_inputFOVSpinBox_valueChanged(int /*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImage();
}

void MainWindow::on_VFovSpinBox_valueChanged(int /*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImage();
}

void MainWindow::on_HFovSpinBox_valueChanged(int /*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImage();
}

void MainWindow::on_GUIPreviewInterpolation_currentTextChanged(const QString &/*arg1*/) {
	if (ui->CameraFrame->isVisible()) ReloadFilterImage();
}

void MainWindow::on_FfmpegBinSelect_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Select ffmpeg binary", "", "All Files (*)");
	if (File != "") ui->FfmpegBin->setText(File);
}

void MainWindow::on_ClearErrorLog_clicked() {
	ui->ErrorLog->clear();
	ui->LeftTabs->setCurrentIndex(LEFT_TAB_CAMERA);
	ui->LeftTabs->setTabVisible(LEFT_TAB_ERROR_LOG, false);
}

void MainWindow::on_VideoLength_userTimeChanged(const QTime &time) {
	if (time.msecsSinceStartOfDay() == 0) {
		ui->RawTime->clearMaximumTime();
	} else {
		QTime MaxTime = time.addMSecs((-4000 / ui->FPSSpinBox->value()) - 1);
		ui->RawTime->setMaximumTime(MaxTime);
		ui->PreviewStart->setMaximumTime(MaxTime);
		ui->PreviewEnd->setMaximumTime(MaxTime);
	}
}

void MainWindow::on_SelectVideoPlayer_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Select video player binary", "", "All Files (*)");
	if (File != "") ui->VideoPlayer->setText(File);
}

void MainWindow::on_SelectFfprobe_clicked() {
	QString File = QFileDialog::getOpenFileName(this, "Select ffprobe binary", "", "All Files (*)");
	if (File != "") ui->FfprobeBin->setText(File);
}

void MainWindow::on_Sort_clicked() {
	TableUndo->clear();
	ui->Table->sortByColumn(TIME_COLUMN, Qt::AscendingOrder);
	for (int i = 0; i < ui->Table->rowCount(); ++i) {
		tableline_t Line = ReadRow(i);
	}
}

void MainWindow::on_Cancel_clicked() {
	ExecutionCanceled = true;
	Execution.close();
}

void MainWindow::on_actionSave_screen_triggered() {

}

void MainWindow::on_PreviewCurrentLine_clicked() {
	on_PreviewTransition_clicked();
	ExecuteProcess();
}

void MainWindow::on_RAMCache_valueChanged(int arg1) {
	RawCache.setMaxCost(arg1 * 1024 * 1024);
}

void MainWindow::on_GenerateDiskCache_clicked() {
	GenerateDiskCache(60);
	DiskCacheProcessTerminate(0, QProcess::NormalExit);
}


void MainWindow::on_Gen10SCache_clicked() {
	GenerateDiskCache(10);
	DiskCacheProcessTerminate(0, QProcess::NormalExit);
}

void MainWindow::on_ClearDiskCache_clicked() {
	CleanDiskCache(0);
	ui->CurrentDiskUsage->setText(QString::number(0, 'f', 2));
}

void MainWindow::on_ClearRAMCache_clicked() {
	RawCache.clear();
	ui->CurrentRAMUsage->setText(QString::number(0, 'f', 2));
}


void MainWindow::on_DiskCacheDirSelect_clicked() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  tr("Open Cache Directory"),
				  QDir::tempPath(),
				  QFileDialog::ShowDirsOnly
				  | QFileDialog::DontResolveSymlinks);

	if (Dir != "") ui->DiskCacheDir->setText(Dir);
}


void MainWindow::on_TakeScreenshot1920_clicked() {
	TakeScreenshot(ui->RawTime->time(), 1920, 1080, 0, 0);
}

void MainWindow::on_TakeScreenshot1280_clicked() {
	TakeScreenshot(ui->RawTime->time(), 1280, 720, 0, 0);
}

void MainWindow::on_TakeScreenshot_clicked() {
	TakeScreenshot(
				ui->RawTime->time(),
				ui->ScreenshotsV360W->value(),
				ui->ScreenshotsV360H->value(),
				ui->ScreenshotsW->value(),
				ui->ScreenshotsH->value());
}

void MainWindow::on_TakeScreenshotRescaled_clicked() {
	TakeScreenshot(ui->RawTime->time(), 1280, 720, 1920, 1080);
}

void MainWindow::on_ShowFramingLines_stateChanged(int /*arg1*/) {
	if (FilterPixmap.isNull()) return;
	ScaledFilterPixmap = FilterPixmap.scaledToHeight(ui->CameraFrame->height());
	if (ui->ShowFramingLines->isChecked()) PrintFramingLines();
	ui->CameraFrame->setPixmap(ScaledFilterPixmap);
}

void MainWindow::on_PreviewDirSelect_clicked() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  tr("Open Previews Directory"),
				  QDir::tempPath(),
				  QFileDialog::ShowDirsOnly
				  | QFileDialog::DontResolveSymlinks);

	if (Dir != "") ui->PreviewDir->setText(Dir);
}

void MainWindow::on_pushButton_clicked() {
	QString Dir = QFileDialog::getExistingDirectory(this,
				  tr("Open Screenshots Directory"),
				  QDir::tempPath(),
				  QFileDialog::ShowDirsOnly
				  | QFileDialog::DontResolveSymlinks);

	if (Dir != "") ui->ScreenshotsDir->setText(Dir);
}

void MainWindow::on_EnableDebug_stateChanged(int arg1) {
	if (arg1 == Qt::Checked) {
		Debug = true;
		ui->RightTabs->setTabVisible(RIGHT_TAB_DEBUG, true);
	} else {
		Debug = false;
		ui->RightTabs->setTabVisible(RIGHT_TAB_DEBUG, false);
	}
}

